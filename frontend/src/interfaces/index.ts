export interface UserProfile {
    email: string;
    disabled: boolean;
    role: string;
    firstName: string;
    lastName: string;
    id: string;
}

export interface UserProfileUpdate {
    email?: string;
    firstName?: string;
    lastName?: string;
    password?: string;
    disabled?: boolean;
    role?: string;
}
