import { request } from './axios'

const searchUsers = async (payload: any): Promise<any> => {
  return await request.get('/users/', { params: payload })
}

const getUserProfile = async (id: number): Promise<any> => {
  return await request.get(`/users/${id}/`)
}

const changeUser = async (id: number, payload: any): Promise<any> => {
  return await request.patch(`/users/${id}/`, payload)
}

const createUser = async (payload: any): Promise<any> => {
  return await request.post('/users/', payload)
}

export const UsersApi = {
  searchUsers,
  getUserProfile,
  changeUser,
  createUser
}
