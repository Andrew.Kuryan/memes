import { request } from './axios'

const searchAll = async (query: any): Promise<any> => {
  return await request.get('/memes/', { params: query })
}

const getOne = async (id: number): Promise<any> => {
  return await request.get(`/memes/${id}/`)
}

const changeOne = async (id: number, payload: any): Promise<any> => {
  return await request.patch(`/memes/${id}/`, payload)
}

const createOne = async (payload: any): Promise<any> => {
  return await request.post('/memes/', payload)
}

const deleteOne = async (id: number): Promise<any> => {
  return await request.delete(`/memes/${id}/`)
}

export const MemesApi = {
  searchAll,
  getOne,
  changeOne,
  createOne,
  deleteOne
}
