import { request } from './axios'

const searchAll = async (query: any): Promise<any> => {
  return await request.get('/tags/', { params: query })
}

const getOne = async (name: string): Promise<any> => {
  return await request.get(`/tags/${name}/`)
}

const changeOne = async (id: number, payload: any): Promise<any> => {
  return await request.patch(`/tags/${id}/`, payload)
}

const createOne = async (payload: any): Promise<any> => {
  return await request.post('/tags/', payload)
}

const deleteOne = async (name: string): Promise<any> => {
  return await request.delete(`/tags/${name}/`)
}

export const TagsApi = {
  searchAll,
  getOne,
  changeOne,
  createOne,
  deleteOne
}
