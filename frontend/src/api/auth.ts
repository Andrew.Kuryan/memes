import { request } from './axios'

const getToken = async (username: string, password: string): Promise<any> => {
  const params = new URLSearchParams()
  params.append('username', username)
  params.append('password', password)
  params.append('grant_type', 'password')
  params.append('client_secret', 'memes frontend app')
  params.append('client_id', '1')
  return await request.post('/oauth/token/', params)
}

const setToken = function (refresh: string, access: string): string {
  localStorage.setItem('refresh', refresh)
  localStorage.setItem('token', access)
  return access
}

const logout = function (): string {
  localStorage.removeItem('token')
  localStorage.removeItem('refresh')
  return ''
}

const checkAuth = function (): string {
  const token = localStorage.getItem('token')
  return token || logout()
}

export const AuthApi = {
  getToken,
  setToken,
  checkAuth,
  logout
}
