import { request } from './axios'

const searchAll = async (query: any): Promise<any> => {
  return await request.get('/templates/', { params: query })
}

const getOne = async (id: number): Promise<any> => {
  return await request.get(`/templates/${id}/`)
}

const changeOne = async (id: number, payload: any): Promise<any> => {
  return await request.patch(`/templates/${id}/`, payload)
}

const createOne = async (payload: any): Promise<any> => {
  return await request.post('/templates/', payload)
}

const deleteOne = async (id: number): Promise<any> => {
  return await request.delete(`/templates/${id}/`)
}

export const TemplatesApi = {
  searchAll,
  getOne,
  changeOne,
  createOne,
  deleteOne
}
