import axios from 'axios'

import { AuthApi } from './auth'

export const request = axios.create({
  baseURL: `${process.env.VUE_APP_URL}/api/v1/`
})

// passive effects
request.interceptors.request.use(
  config => {
    const token = localStorage.getItem('token')
    if (token !== null) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    if (error.response.status === 401) {
      AuthApi.logout()
    }
    return Promise.reject(error)
  }
)
