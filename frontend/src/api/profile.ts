import { request } from './axios'

const editProfile = async (id: number, newFields: any): Promise<any> => {
  return await request.patch(`/users/${id}/`, newFields)
}

const getMyProfile = async (): Promise<any> => await request.get('/users/me/')

const checkProfile = async (): Promise<boolean> => {
  return (
    (await getMyProfile()
      .then(({ data }) => data)
      .catch(() => null)) !== null
  )
}

export const ProfileApi = {
  editProfile,
  checkProfile,
  getMyProfile
}
