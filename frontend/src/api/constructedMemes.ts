import { request } from './axios'

const searchAll = async (query: any): Promise<any> => {
  return await request.get('/constructed-memes/', { params: query })
}

const getOne = async (id: number): Promise<any> => {
  return await request.get(`/constructed-memes/${id}/`)
}

const changeOne = async (id: number, payload: any): Promise<any> => {
  return await request.patch(`/constructed-memes/${id}/`, payload)
}

const createOne = async (payload: any): Promise<any> => {
  return await request.post('/constructed-memes/', payload)
}

const deleteOne = async (id: number): Promise<any> => {
  return await request.delete(`/constructed-memes/${id}/`)
}

export const ConstructedMemesApi = {
  searchAll,
  getOne,
  changeOne,
  createOne,
  deleteOne
}
