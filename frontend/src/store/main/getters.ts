import { MainState } from '@/store/main/state'
import { getStoreAccessors } from 'typesafe-vuex'
import { State } from '@/store/state'

export const getters = {
  token: (state: MainState) => state.token,
  isLoggedIn: (state: MainState) => state.isLoggedIn,
  firstNotification: (state: MainState) => state.notifications.length > 0 && state.notifications[0],
  loginError: (state: MainState) => state.logInError
}

const { read } = getStoreAccessors<MainState, State>('')

export const readIsLoggedIn = read(getters.isLoggedIn)
export const readToken = read(getters.token)
export const readFirstNotification = read(getters.firstNotification)
export const readLoginError = read(getters.loginError)
