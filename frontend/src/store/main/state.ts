import { UserProfile } from '@/interfaces'

export interface AppNotification {
    content: string;
    color?: string;
    showProgress?: boolean;
}

export interface MainState {
    token: string;
    isLoggedIn: boolean | null;
    logInError: boolean;
    userProfile: UserProfile | null;
    notifications: AppNotification[];
}
