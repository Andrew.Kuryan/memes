import { getStoreAccessors } from 'typesafe-vuex'
import { MainState } from '@/store/main/state'
import { State } from '@/store/state'
import { actions } from '@/store/main/actions'

const { dispatch } = getStoreAccessors<MainState | any, State>('')

export const dispatchCheckApiError = dispatch(actions.actionCheckApiError)
export const dispatchGetUserProfile = dispatch(actions.actionGetUserProfile)
export const dispatchRouteLoggedIn = dispatch(actions.actionRouteLoggedIn)
export const dispatchCheckLoggedIn = dispatch(actions.actionCheckLoggedIn)
export const dispatchLogIn = dispatch(actions.actionLogIn)
export const dispatchLogOut = dispatch(actions.actionLogOut)
export const dispatchUserLogOut = dispatch(actions.actionUserLogOut)
export const dispatchRemoveLogIn = dispatch(actions.actionRemoveLogIn)
export const dispatchRouteLogOut = dispatch(actions.actionRouteLogOut)
export const dispatchRemoveNotification = dispatch(actions.removeNotification)
