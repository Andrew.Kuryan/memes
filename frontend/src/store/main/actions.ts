import router from '@/router'
import { AxiosError } from 'axios'
import { ActionContext } from 'vuex'
import { State } from '../state'
import {
  commitAddNotification,
  commitRemoveNotification,
  commitSetLoggedIn,
  commitSetLogInError,
  commitSetToken,
  commitSetUserProfile
} from './mutations'
import { AppNotification, MainState } from './state'
import { ProfileApi } from '@/api/profile'
import { AuthApi } from '@/api/auth'
import {
  dispatchCheckApiError,
  dispatchGetUserProfile,
  dispatchLogOut,
  dispatchRemoveLogIn,
  dispatchRouteLoggedIn,
  dispatchRouteLogOut
} from '@/store/main/dispatchActions'

type MainContext = ActionContext<MainState, State>

export const actions = {
  async actionGetUserProfile (context: MainContext) {
    try {
      const response = await ProfileApi.getMyProfile()
      if (response.data) {
        commitSetUserProfile(context, response.data)
      }
    } catch (error) {
      await dispatchCheckApiError(context, error)
    }
  },
  async actionLogIn (context: MainContext, payload: { username: string; password: string }) {
    try {
      let token = null
      AuthApi.getToken(payload.username, payload.password).then(
        ({ data }) => {
          token = AuthApi.setToken(data.refresh, data.access)
        }
      )
      if (token) {
        localStorage.setItem('token', token)
        commitSetToken(context, token)
        commitSetLoggedIn(context, true)
        commitSetLogInError(context, false)
        await dispatchGetUserProfile(context)
        await dispatchRouteLoggedIn(context)
        commitAddNotification(context, { content: 'Logged in', color: 'success' })
      } else {
        await dispatchLogOut(context)
      }
    } catch (err) {
      commitSetLogInError(context, true)
      await dispatchLogOut(context)
    }
  },
  async actionCheckLoggedIn (context: MainContext) {
    if (!context.state.isLoggedIn) {
      let token = context.state.token
      if (!token) {
        const localToken = localStorage.getItem('token')
        if (localToken) {
          commitSetToken(context, localToken)
          token = localToken
        }
      }
      if (await ProfileApi.checkProfile()) {
        try {
          const response = await ProfileApi.getMyProfile()
          commitSetLoggedIn(context, true)
          commitSetUserProfile(context, response.data)
        } catch (error) {
          await dispatchRemoveLogIn(context)
        }
      } else {
        await dispatchRemoveLogIn(context)
      }
    }
  },
  async actionRemoveLogIn (context: MainContext) {
    localStorage.removeItem('token')
    commitSetToken(context, '')
    commitSetLoggedIn(context, false)
  },
  async actionLogOut (context: MainContext) {
    await dispatchRemoveLogIn(context)
    await dispatchRouteLogOut(context)
  },
  async actionUserLogOut (context: MainContext) {
    await dispatchLogOut(context)
    commitAddNotification(context, { content: 'Logged out', color: 'success' })
  },
  actionRouteLogOut (context: MainContext) {
    if (router.currentRoute.path !== '/login') {
      router.push('/login').then()
    }
  },
  async actionCheckApiError (context: MainContext, payload: AxiosError) {
    if (payload.response!.status === 401) {
      await dispatchLogOut(context)
    }
  },
  actionRouteLoggedIn (context: MainContext) {
    if (router.currentRoute.path === '/login') {
      router.push('/').then()
    }
  },
  async removeNotification (context: MainContext, payload: { notification: AppNotification; timeout: number }) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commitRemoveNotification(context, payload.notification)
        resolve(true)
      }, payload.timeout)
    })
  }
}
