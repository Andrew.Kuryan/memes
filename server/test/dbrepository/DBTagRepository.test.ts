import * as shortid from 'shortid';

import Tag from '../../src/entity/Tag';
import DBTagRepository from '../../src/dbrepository/DBTagRepository';

describe('DBTagRepository', () => {
    const tagRepository = new DBTagRepository();

    const testTag = () => new Tag(shortid.generate());

    it('should create new tag', async done => {
        const tag = testTag();
        const end = async () => {
            await tagRepository.remove(tag);
        };

        try {
            await tagRepository.create(tag);
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });

    it('should get all tags', async done => {
        try {
            const tags = await tagRepository.getAll({ limit: 25, page: 1 });
            expect(tags.result).toHaveLength(4);
            expect(tags.totalCount).toEqual(4);
            for (let i = 0; i < tags.result.length; i++) {
                expect(tags.result[i].getName()).toBeDefined();
            }
        } catch (exc) {
            done.fail(exc);
        }
        done();
    });

    it('should check if tag exists', async done => {
        const tag = testTag();
        const end = async () => {
            await tagRepository.remove(tag);
        };

        try {
            await tagRepository.create(tag);
            const result = await tagRepository.exists(tag.getName());
            expect(result).toEqual(true);
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });

    it('should check if tag not exists', async done => {
        const tag = testTag();
        const end = async () => {
            await tagRepository.remove(tag);
        };

        try {
            await tagRepository.create(tag);
            const result = await tagRepository.exists(tag.getName() + 'nonexistent');
            expect(result).toEqual(false);
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });
});
