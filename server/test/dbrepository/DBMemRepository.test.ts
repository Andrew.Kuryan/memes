import DBMemRepository from '../../src/dbrepository/DBMemRepository';
import { ImageData, ImageFilter } from '../../src/entity/Image';
import User from '../../src/entity/User';
import Tag from '../../src/entity/Tag';
import DBUserRepository from '../../src/dbrepository/DBUserRepository';
import { MemFactory } from '../../src/entity/Mem';
import {
    createTest,
    getByIdNegativeTest,
    getByIdTest,
    searchTest,
    updateTest,
} from './DBImageRepository.utils';

const testAuthor = new User({
    login: 'TestUserForMemRepositoryTest',
    password: 'test123',
    email: 'test-user-for-mem-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testLiker = new User({
    login: 'TestLikerForMemRepositoryTest',
    password: 'test123',
    email: 'test-liker-for-mem-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testImageData = new ImageData({
    author: testAuthor,
    imageUrl: 'test-url',
    tags: [new Tag('Relations'), new Tag('Programming')],
    likedUsers: [testLiker],
});

describe('DBMemRepository', () => {
    const userRepository = new DBUserRepository();
    const memRepository = new DBMemRepository();

    beforeAll(async done => {
        await userRepository.create(testAuthor);
        await userRepository.create(testLiker);
        done();
    });

    afterAll(async done => {
        await userRepository.remove(testAuthor);
        await userRepository.remove(testLiker);
        done();
    });

    it('should create new mem', createTest(memRepository, testImageData));

    it('should get mem by id', getByIdTest(memRepository, testImageData));

    it('should throw error if no such mem', getByIdNegativeTest(memRepository, testImageData));

    it(
        'should update existing mem',
        updateTest(
            memRepository,
            testImageData,
            new ImageData({
                author: testLiker,
                imageUrl: 'test-url-updated',
                tags: [new Tag('Relations'), new Tag('Films')],
                likedUsers: [testAuthor],
            }),
            new MemFactory(),
        ),
    );

    it(
        'should search memes with ImageFilter',
        searchTest(
            memRepository,
            testImageData,
            new ImageFilter({
                authorLogin: 'Test',
                creationDateFrom: new Date(2019, 5, 1, 0, 0, 0, 0),
                creationDateTo: new Date(2020, 1, 1, 0, 0, 0, 0),
                tagsName: ['Relations', 'NonExisting'],
                likedUsersLogin: ['Test', 'Test1'],
            }),
            [2],
        ),
    );
});
