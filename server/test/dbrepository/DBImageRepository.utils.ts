import Image, { ImageData, ImageFactory, ImageFilter } from '../../src/entity/Image';
import ImageRepository from '../../src/appports/repository/ImageRepository';
import { compareImages } from '../utils';
import DoneCallback = jest.DoneCallback;

export const createTest = (
    imageRepository: ImageRepository<Image>,
    testImageData: ImageData,
) => async (done: DoneCallback) => {
    try {
        const result = await imageRepository.create(testImageData);
        expect(result.getId()).toBeDefined();
        expect(result.getCreationDate()).toBeDefined();
        await imageRepository.remove(result);
        done();
    } catch (exc) {
        done.fail(exc);
    }
};

export const getByIdTest = (
    imageRepository: ImageRepository<Image>,
    testImageData: ImageData,
) => async (done: DoneCallback) => {
    try {
        const image = await imageRepository.create(testImageData);

        try {
            const result = await imageRepository.getById(image.getId());
            compareImages(result, image);
            await imageRepository.remove(image);
            done();
        } catch (exc) {
            await imageRepository.remove(image);
            done.fail(exc);
        }
    } catch (exc) {
        done.fail(exc);
    }
};

export const getByIdNegativeTest = (
    imageRepository: ImageRepository<Image>,
    testImageData: ImageData,
) => async () => {
    expect.assertions(1);
    const image = await imageRepository.create(testImageData);

    try {
        await imageRepository.getById(image.getId() + 5);
    } catch (exc) {
        await imageRepository.remove(image);
        expect(exc.message).toEqual('No such image');
    }

    await imageRepository.remove(image);
};

export const updateTest = (
    imageRepository: ImageRepository<Image>,
    testImageData: ImageData,
    newTestData: ImageData,
    imageFactory: ImageFactory,
) => async (done: DoneCallback) => {
    try {
        const image = await imageRepository.create(testImageData);
        try {
            await imageRepository.update(image, newTestData);
            const result = await imageRepository.getById(image.getId());
            compareImages(
                result,
                imageFactory.create({
                    id: image.getId(),
                    creationDate: image.getCreationDate(),
                    data: newTestData,
                }),
            );
            await imageRepository.remove(image);
            done();
        } catch (exc) {
            await imageRepository.remove(image);
            done.fail(exc);
        }
    } catch (exc) {
        done.fail(exc);
    }
};

export const searchTest = (
    imageRepository: ImageRepository<Image>,
    testImageData: ImageData,
    testFilter: ImageFilter,
    expectedResult: Array<number>,
) => async (done: DoneCallback) => {
    try {
        const image = await imageRepository.create(testImageData);
        try {
            const res = await imageRepository.search(testFilter, { limit: 25, page: 1 });
            expect(res.result.length).toEqual(expectedResult.length);
            for (let i = 0; i < expectedResult.length; i++) {
                expect(res.result[i].getId()).toEqual(expectedResult[i]);
            }
            await imageRepository.remove(image);
            done();
        } catch (exc) {
            await imageRepository.remove(image);
            done.fail(exc);
        }
    } catch (exc) {
        done.fail(exc);
    }
};
