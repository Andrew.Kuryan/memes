import OAuthTokenRepository from '../../src/appports/repository/OAuthTokenRepository';
import OAuthToken, { OAuthTokenData } from '../../src/entity/OAuthToken';
import DoneCallback = jest.DoneCallback;
import { compareOAuthTokens } from '../utils';

export const createTest = (
    tokenRepository: OAuthTokenRepository<OAuthToken>,
    getTestTokenData: () => OAuthTokenData,
) => async (done: DoneCallback) => {
    const testTokenData = getTestTokenData();
    try {
        const result = await tokenRepository.create(testTokenData);
        expect(result.getId()).toBeDefined();
        await tokenRepository.remove(result);
        done();
    } catch (exc) {
        done.fail(exc);
    }
};

export const getByTokenTest = (
    tokenRepository: OAuthTokenRepository<OAuthToken>,
    getTestTokenData: () => OAuthTokenData,
) => async (done: DoneCallback) => {
    const testTokenData = getTestTokenData();
    try {
        const token = await tokenRepository.create(testTokenData);

        try {
            const result = await tokenRepository.getByToken(token.getData().getToken());
            expect(result).not.toBeNull();
            compareOAuthTokens(result as OAuthToken, token as OAuthToken);
            await tokenRepository.remove(token);
            done();
        } catch (exc) {
            await tokenRepository.remove(token);
            done.fail(exc);
        }
    } catch (exc) {
        done.fail(exc);
    }
};

export const getByTokenNegativeTest = (
    tokenRepository: OAuthTokenRepository<OAuthToken>,
    getTestTokenData: () => OAuthTokenData,
) => async (done: DoneCallback) => {
    const testTokenData = getTestTokenData();
    try {
        const token = await tokenRepository.create(testTokenData);

        try {
            const result = await tokenRepository.getByToken(
                token.getData().getToken() + 'nonexistent',
            );
            expect(result).toBeNull();
            await tokenRepository.remove(token);
            done();
        } catch (exc) {
            await tokenRepository.remove(token);
            done.fail(exc);
        }
    } catch (exc) {
        done.fail(exc);
    }
};
