import DBOAuthClientRepository from '../../src/dbrepository/DBOAuthClientRepository';
import OAuthClient, { OAuthClientData } from '../../src/entity/OAuthClient';
import { compareOauthClients } from '../utils';

describe('DBOAuthClientRepository', () => {
    const oAuthClientRepository = new DBOAuthClientRepository();

    const clientData = new OAuthClientData({
        name: 'Test Client',
        accessTokenLifetime: 3600,
        refreshTokenLifetime: 7200,
        clientSecret: 'secret',
        grantTypes: ['password'],
    });

    it('should create new oauth client', async done => {
        try {
            const result = await oAuthClientRepository.create(clientData);
            expect(result.getId()).toBeDefined();
            await oAuthClientRepository.remove(result);
            done();
        } catch (exc) {
            done.fail(exc);
        }
    });

    it('should get oauth client by id', async done => {
        try {
            const client = await oAuthClientRepository.create(clientData);

            try {
                const result = await oAuthClientRepository.getById(client.getId());
                expect(result).not.toBeNull();
                compareOauthClients(result as OAuthClient, client as OAuthClient);
            } catch (exc) {
                await oAuthClientRepository.remove(client);
                done.fail(exc);
            }

            await oAuthClientRepository.remove(client);
            done();
        } catch (exc) {
            done.fail(exc);
        }
    });

    it('should return null if client does not exists', async done => {
        try {
            const client = await oAuthClientRepository.create(clientData);

            try {
                const result = await oAuthClientRepository.getById(client.getId() + 1);
                expect(result).toBeNull();
            } catch (exc) {
                await oAuthClientRepository.remove(client);
                done.fail(exc);
            }

            await oAuthClientRepository.remove(client);
            done();
        } catch (exc) {
            done.fail(exc);
        }
    });
});
