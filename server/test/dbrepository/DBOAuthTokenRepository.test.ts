import { createTest, getByTokenNegativeTest, getByTokenTest } from './DBOauthTokenRepository.utils';
import {
    DBAccessTokenRepository,
    DBRefreshTokenRepository,
} from '../../src/dbrepository/DBOAuthTokenRepository';
import { OAuthTokenData } from '../../src/entity/OAuthToken';
import User from '../../src/entity/User';
import Tag from '../../src/entity/Tag';
import OAuthClient, { OAuthClientData } from '../../src/entity/OAuthClient';
import DBUserRepository from '../../src/dbrepository/DBUserRepository';
import DBOAuthClientRepository from '../../src/dbrepository/DBOAuthClientRepository';

const testClientData = new OAuthClientData({
    name: 'Application',
    clientSecret: 'secret',
    grantTypes: ['password', 'refresh_token'],
    accessTokenLifetime: 3600,
    refreshTokenLifetime: 7200,
});

describe('DBAccessTokenRepository', () => {
    const accessTokenRepository = new DBAccessTokenRepository();
    const userRepository = new DBUserRepository();
    const oAuthClientRepository = new DBOAuthClientRepository();

    const testUser = new User({
        login: 'TestUserForAccessTokenRepositoryTest',
        password: 'test123',
        email: 'test-user-for-access-token-repository-test@test.com',
        subscriptions: [new Tag('Relations'), new Tag('Films')],
    });

    let testClient: OAuthClient;
    let testTokenData: OAuthTokenData;

    beforeAll(async done => {
        await userRepository.create(testUser);
        testClient = await oAuthClientRepository.create(testClientData);
        testTokenData = new OAuthTokenData({
            token: '987654321',
            tokenExpiresOn: new Date(2022, 7, 10),
            user: testUser,
            client: testClient,
        });
        done();
    });

    afterAll(async done => {
        await oAuthClientRepository.remove(testClient);
        await userRepository.remove(testUser);
        done();
    });

    it(
        'should create new access token',
        createTest(accessTokenRepository, () => testTokenData),
    );

    it(
        'should get access token by token',
        getByTokenTest(accessTokenRepository, () => testTokenData),
    );

    it(
        'should return null if token does not exists',
        getByTokenNegativeTest(accessTokenRepository, () => testTokenData),
    );
});

describe('DBRefreshTokenRepository', () => {
    const refreshTokenRepository = new DBRefreshTokenRepository();
    const userRepository = new DBUserRepository();
    const oAuthClientRepository = new DBOAuthClientRepository();

    const testUser = new User({
        login: 'TestUserForRefreshTokenRepositoryTest',
        password: 'test123',
        email: 'test-user-for-refresh-token-repository-test@test.com',
        subscriptions: [new Tag('Relations'), new Tag('Films')],
    });

    let testClient: OAuthClient;
    let testTokenData: OAuthTokenData;

    beforeAll(async done => {
        await userRepository.create(testUser);
        testClient = await oAuthClientRepository.create(testClientData);
        testTokenData = new OAuthTokenData({
            token: '123456789',
            tokenExpiresOn: new Date(2022, 7, 10),
            user: testUser,
            client: testClient,
        });
        done();
    });

    afterAll(async done => {
        await oAuthClientRepository.remove(testClient);
        await userRepository.remove(testUser);
        done();
    });

    it(
        'should create new refresh token',
        createTest(refreshTokenRepository, () => testTokenData),
    );

    it(
        'should get refresh token by token',
        getByTokenTest(refreshTokenRepository, () => testTokenData),
    );

    it(
        'should return null if token does not exists',
        getByTokenNegativeTest(refreshTokenRepository, () => testTokenData),
    );
});
