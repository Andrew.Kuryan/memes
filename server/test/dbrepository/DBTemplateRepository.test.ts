import DBUserRepository from '../../src/dbrepository/DBUserRepository';
import {
    createTest,
    getByIdNegativeTest,
    getByIdTest,
    searchTest,
    updateTest,
} from './DBImageRepository.utils';
import DBTemplateRepository from '../../src/dbrepository/DBTemplateRepository';
import { ImageData, ImageFilter } from '../../src/entity/Image';
import Tag from '../../src/entity/Tag';
import { TemplateFactory } from '../../src/entity/Template';
import User from '../../src/entity/User';

const testAuthor = new User({
    login: 'TestUserForTemplateRepositoryTest',
    password: 'test123',
    email: 'test-user-for-template-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testLiker = new User({
    login: 'TestLikerForTemplateRepositoryTest',
    password: 'test123',
    email: 'test-liker-for-template-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testImageData = new ImageData({
    author: testAuthor,
    imageUrl: 'test-url',
    tags: [new Tag('Relations'), new Tag('Programming')],
    likedUsers: [testLiker],
});

describe('DBTemplateRepository', () => {
    const userRepository = new DBUserRepository();
    const templateRepository = new DBTemplateRepository();

    beforeAll(async done => {
        await userRepository.create(testAuthor);
        await userRepository.create(testLiker);
        done();
    });

    afterAll(async done => {
        await userRepository.remove(testAuthor);
        await userRepository.remove(testLiker);
        done();
    });

    it('should create new template', createTest(templateRepository, testImageData));

    it('should get template by id', getByIdTest(templateRepository, testImageData));

    it(
        'should throw error if no such template',
        getByIdNegativeTest(templateRepository, testImageData),
    );

    it(
        'should update existing template',
        updateTest(
            templateRepository,
            testImageData,
            new ImageData({
                author: testLiker,
                imageUrl: 'test-url-updated',
                tags: [new Tag('Relations'), new Tag('Programming'), new Tag('Films')],
                likedUsers: [testAuthor],
            }),
            new TemplateFactory(),
        ),
    );

    it(
        'should search memes with ImageFilter',
        searchTest(
            templateRepository,
            testImageData,
            new ImageFilter({
                authorLogin: 'Test',
                creationDateFrom: new Date(2020, 0, 1, 0, 0, 0, 0),
                creationDateTo: new Date(2020, 1, 1, 0, 0, 0, 0),
                tagsName: ['Relations', 'NonExisting', 'Films'],
                likedUsersLogin: ['Test', 'Test1'],
            }),
            [5],
        ),
    );
});
