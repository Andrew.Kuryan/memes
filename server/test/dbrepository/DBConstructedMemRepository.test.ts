import User from '../../src/entity/User';
import Tag from '../../src/entity/Tag';
import { ImageData } from '../../src/entity/Image';
import DBUserRepository from '../../src/dbrepository/DBUserRepository';
import DBConstructedMemRepository from '../../src/dbrepository/DBConstructedMemRepository';
import ConstructedMem, {
    ConstructedMemData,
    ConstructedMemFilter,
} from '../../src/entity/ConstructedMem';
import DBTemplateRepository from '../../src/dbrepository/DBTemplateRepository';
import Template from '../../src/entity/Template';
import { compareConstructedMemes } from '../utils';

const testAuthor = new User({
    login: 'TestUserForConstructedMemRepositoryTest',
    password: 'test123',
    email: 'test-user-for-constructed-mem-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testLiker = new User({
    login: 'TestLikerForConstructedMemRepositoryTest',
    password: 'test123',
    email: 'test-liker-for-constructed-mem-repository-test@test.com',
    subscriptions: [new Tag('Relations'), new Tag('Films')],
});

const testTemplateData1 = new ImageData({
    author: testAuthor,
    imageUrl: 'test-template1-url',
    tags: [new Tag('Relations')],
    likedUsers: [testLiker],
});

const testTemplateData2 = new ImageData({
    author: testLiker,
    imageUrl: 'test-template2-url',
    tags: [new Tag('Films')],
    likedUsers: [testAuthor],
});

const testTemplateData3 = new ImageData({
    author: testLiker,
    imageUrl: 'test-template3-url',
    tags: [new Tag('Programming')],
    likedUsers: [testAuthor],
});

let template1: Template;
let template2: Template;
let template3: Template;
let testConstructedMemData: ConstructedMemData;

describe('DBConstructedMemRepository', () => {
    const userRepository = new DBUserRepository();
    const templateRepository = new DBTemplateRepository();
    const constructedMemRepository = new DBConstructedMemRepository();

    beforeAll(async () => {
        await userRepository.create(testAuthor);
        await userRepository.create(testLiker);

        template1 = await templateRepository.create(testTemplateData1);
        template2 = await templateRepository.create(testTemplateData2);
        template3 = await templateRepository.create(testTemplateData3);

        testConstructedMemData = new ConstructedMemData({
            author: testAuthor,
            imageUrl: 'test-url',
            tags: [new Tag('Relations'), new Tag('Programming')],
            likedUsers: [testLiker],
            templates: [template1, template2],
        });
    });

    afterAll(async () => {
        await templateRepository.remove(template3);
        await templateRepository.remove(template2);
        await templateRepository.remove(template1);

        await userRepository.remove(testLiker);
        await userRepository.remove(testAuthor);
    });

    it('should create new constructed mem', async done => {
        try {
            const result = await constructedMemRepository.create(testConstructedMemData);
            expect(result.getId()).toBeDefined();
            expect(result.getCreationDate()).toBeDefined();
            await constructedMemRepository.remove(result);
            done();
        } catch (exc) {
            done.fail(exc);
        }
    });

    it('should get constructed mem by id', async done => {
        try {
            const constructedMem = await constructedMemRepository.create(testConstructedMemData);

            try {
                const result = await constructedMemRepository.getById(constructedMem.getId());
                compareConstructedMemes(result, constructedMem);
                await constructedMemRepository.remove(constructedMem);
                done();
            } catch (exc) {
                await constructedMemRepository.remove(constructedMem);
                done.fail(exc);
            }
        } catch (exc) {
            done.fail(exc);
        }
    });

    it('should update existing constructed mem', async done => {
        const newTestData = new ConstructedMemData({
            author: testLiker,
            imageUrl: 'updated-test-mem-url',
            tags: [new Tag('Films')],
            likedUsers: [testAuthor],
            templates: [template2, template3],
        });

        try {
            const image = await constructedMemRepository.create(testConstructedMemData);
            try {
                await constructedMemRepository.update(image, newTestData);
                const result = await constructedMemRepository.getById(image.getId());
                compareConstructedMemes(
                    result,
                    new ConstructedMem({
                        id: image.getId(),
                        creationDate: image.getCreationDate(),
                        data: newTestData,
                    }),
                );
                await constructedMemRepository.remove(image);
                done();
            } catch (exc) {
                await constructedMemRepository.remove(image);
                done.fail(exc);
            }
        } catch (exc) {
            done.fail(exc);
        }
    });

    it('should search constructed memes with ImageFilter', async done => {
        try {
            const testFilter = new ConstructedMemFilter({
                authorLogin: 'Test',
                creationDateFrom: new Date(2020, 1, 1, 0, 0, 0, 0),
                creationDateTo: new Date(2020, 2, 1, 0, 0, 0, 0),
                tagsName: ['Programming', 'NonExisting'],
                likedUsersLogin: ['Test', 'Test1'],
                templatesId: [4],
            });

            const image = await constructedMemRepository.create(testConstructedMemData);
            try {
                const result = await constructedMemRepository.search(testFilter, {
                    limit: 25,
                    page: 1,
                });
                expect(result.result.length).toEqual(1);
                expect(result.result[0].getId()).toEqual(3);
                await constructedMemRepository.remove(image);
                done();
            } catch (exc) {
                await constructedMemRepository.remove(image);
                done.fail(exc);
            }
        } catch (exc) {
            done.fail(exc);
        }
    });
});
