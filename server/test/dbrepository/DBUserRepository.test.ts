import * as shortid from 'shortid';

import User from '../../src/entity/User';
import DBUserRepository from '../../src/dbrepository/DBUserRepository';
import Tag from '../../src/entity/Tag';
import { compareUsers } from '../utils';

describe('DBUserRepository', () => {
    const userRepository = new DBUserRepository();

    const testUser = () =>
        new User({
            login: shortid.generate(),
            password: 'test123',
            email: 'test@test.com',
            subscriptions: [new Tag('Films'), new Tag('Relations')],
        });

    it('should create new user', async done => {
        const user = testUser();
        const end = async () => {
            await userRepository.remove(user);
        };

        try {
            const result = await userRepository.create(user);
            expect(result.getLogin()).toEqual(user.getLogin());
            expect(result.getEmail()).toEqual(user.getEmail());
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });

    it('should find user by login', async done => {
        const user = testUser();
        const end = async () => {
            await userRepository.remove(user);
        };

        try {
            await userRepository.create(user);
            const result = await userRepository.getByLogin(user.getLogin());
            compareUsers(result, user);
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });

    it('should throw error if no such user', async () => {
        expect.assertions(1);
        const user = testUser();
        const end = async () => {
            await userRepository.remove(user);
        };

        try {
            await userRepository.create(user);
            await userRepository.getByLogin(user.getLogin() + 'nonexistent');
        } catch (exc) {
            await end();
            expect(exc.message).toEqual('No such user');
        }

        await end();
    });

    it('should update user', async done => {
        const oldUser = testUser();
        const newUser = new User({
            login: oldUser.getLogin(),
            avatarUrl: 'test-url',
            password: 'test123updated',
            email: 'test-updated@test.com',
            subscriptions: [new Tag('Relations'), new Tag('Politics')],
        });
        const end = async () => {
            await userRepository.remove(oldUser);
        };

        try {
            await userRepository.create(oldUser);
            await userRepository.update(oldUser, newUser);
            const result = await userRepository.getByLogin(newUser.getLogin());
            compareUsers(result, newUser);
        } catch (exc) {
            await end();
            done.fail(exc);
        }

        await end();
        done();
    });
});
