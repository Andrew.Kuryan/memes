import Image, { ImageData, ImageFilter } from '../../src/entity/Image';
import User from '../../src/entity/User';
import Tag from '../../src/entity/Tag';

describe('ImageFilter', () => {
    const author = new User({
        login: 'Test',
        password: 'Test',
        email: 'test@gmail.com',
        subscriptions: [new Tag('Programming'), new Tag('Girls')],
    });
    const liker = new User({
        login: 'TestFriend',
        password: 'Test',
        email: 'testfriend@gmail.com',
        subscriptions: [new Tag('Programming'), new Tag('Girls')],
    });

    const image = new Image({
        id: 1,
        creationDate: new Date(2020, 0, 10, 12, 0, 0, 0),
        data: new ImageData({
            author,
            imageUrl: 'test_path',
            tags: [new Tag('Programming'), new Tag('Girls'), new Tag('Films')],
            likedUsers: [author, liker],
        }),
    });

    test('should match(Image) by author', () => {
        const filter = new ImageFilter({
            authorLogin: 'Test',
        });
        expect(filter.match(image)).toEqual(true);
    });

    test('should match(Image) by creationDate', () => {
        const filter = new ImageFilter({
            creationDateFrom: new Date(2020, 0, 1, 0, 0, 0, 0),
            creationDateTo: new Date(2020, 1, 1, 0, 0, 0, 0),
        });
        expect(filter.match(image)).toEqual(true);
    });
    test('should not match(Image) by creationDate', () => {
        const filter = new ImageFilter({
            creationDateFrom: new Date(2020, 0, 11, 0, 0, 0, 0),
        });
        expect(filter.match(image)).toEqual(false);
    });

    test('should match(Image) by tags', () => {
        const filter = new ImageFilter({
            tagsName: ['Girls'],
        });
        expect(filter.match(image)).toEqual(true);
    });
    test('should not match(Image) by tags', () => {
        const filter = new ImageFilter({
            tagsName: ['Demotivators', 'Programming'],
        });
        expect(filter.match(image)).toEqual(false);
    });

    test('should match(Image) by liked users', () => {
        const filter = new ImageFilter({
            likedUsersLogin: ['TestFriend'],
        });
        expect(filter.match(image)).toEqual(true);
    });
});
