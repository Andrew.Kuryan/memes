import { differenceWith } from 'lodash';
import User from '../src/entity/User';
import Tag from '../src/entity/Tag';
import Image from '../src/entity/Image';
import ConstructedMem from '../src/entity/ConstructedMem';
import OAuthClient from '../src/entity/OAuthClient';
import OAuthToken from '../src/entity/OAuthToken';

export function compareArraysWithoutOrder<T, R>(
    arr1: Array<T>,
    arr2: Array<T>,
    compare: (el1: T, el2: T) => boolean,
) {
    expect(differenceWith(arr1, arr2, compare).length).toEqual(0);
}

function deepEqualArraysWithoutOrder<T, R>(
    arr1: Array<T>,
    arr2: Array<T>,
    compare: (el1: T, el2: T) => boolean,
) {
    return differenceWith(arr1, arr2, compare).length === 0;
}

function deepEqualUsers(user1: User, user2: User): boolean {
    return (
        user1.getLogin() === user2.getLogin() &&
        user1.getEmail() === user2.getEmail() &&
        user1.getAvatarUrl() === user2.getAvatarUrl() &&
        deepEqualArraysWithoutOrder(user1.getSubscriptions(), user2.getSubscriptions(), Tag.equals)
    );
}

export function compareUsers(user1: User, user2: User) {
    expect(user1.getLogin()).toEqual(user2.getLogin());
    expect(user1.getEmail()).toEqual(user2.getEmail());
    expect(user1.getAvatarUrl()).toEqual(user2.getAvatarUrl());
    compareArraysWithoutOrder(user1.getSubscriptions(), user2.getSubscriptions(), Tag.equals);
}

function deepEqualImages(image1: Image, image2: Image): boolean {
    return (
        image1.getId() === image2.getId() &&
        image1.getData().getImageUrl() === image2.getData().getImageUrl() &&
        deepEqualUsers(image1.getData().getAuthor(), image2.getData().getAuthor()) &&
        deepEqualArraysWithoutOrder(
            image1.getData().getLikedUsers(),
            image2.getData().getLikedUsers(),
            deepEqualUsers,
        )
    );
}

export function compareImages(image1: Image, image2: Image) {
    expect(image1.getId()).toEqual(image2.getId());
    expect(image1.getCreationDate()).toEqual(image2.getCreationDate());

    expect(image1.getData().getImageUrl()).toEqual(image2.getData().getImageUrl());
    compareUsers(image1.getData().getAuthor(), image2.getData().getAuthor());
    compareArraysWithoutOrder(image1.getData().getTags(), image2.getData().getTags(), Tag.equals);
    compareArraysWithoutOrder(
        image1.getData().getLikedUsers(),
        image2.getData().getLikedUsers(),
        deepEqualUsers,
    );
}

export function compareConstructedMemes(mem1: ConstructedMem, mem2: ConstructedMem) {
    expect(mem1.getId()).toEqual(mem2.getId());
    expect(mem1.getCreationDate()).toEqual(mem2.getCreationDate());

    expect(mem1.getData().getImageUrl()).toEqual(mem2.getData().getImageUrl());
    expect(
        mem1
            .getData()
            .getAuthor()
            .getLogin(),
    ).toEqual(
        mem2
            .getData()
            .getAuthor()
            .getLogin(),
    );

    compareArraysWithoutOrder(mem1.getData().getTags(), mem2.getData().getTags(), Tag.equals);
    compareArraysWithoutOrder(
        mem1.getData().getTemplates(),
        mem2.getData().getTemplates(),
        deepEqualImages,
    );
    compareArraysWithoutOrder(
        mem1.getData().getLikedUsers(),
        mem2.getData().getLikedUsers(),
        deepEqualUsers,
    );
}

export function compareOauthClients(client1: OAuthClient, client2: OAuthClient) {
    expect(client1.getId()).toEqual(client2.getId());

    expect(client1.getData().getName()).toEqual(client2.getData().getName());
    expect(client1.getData().getAccessTokenLifetime()).toEqual(
        client2.getData().getAccessTokenLifetime(),
    );
    expect(client1.getData().getRefreshTokenLifetime()).toEqual(
        client2.getData().getRefreshTokenLifetime(),
    );
    expect(client1.getData().getClientSecret()).toEqual(client2.getData().getClientSecret());
    expect(client1.getData().getRedirectUrl()).toEqual(client2.getData().getRedirectUrl());
    compareArraysWithoutOrder(
        client1.getData().getGrantTypes(),
        client2.getData().getGrantTypes(),
        (s1, s2) => s1 == s2,
    );
}

export function compareOAuthTokens(token1: OAuthToken, token2: OAuthToken) {
    expect(token1.getId()).toEqual(token2.getId());

    expect(token1.getData().getToken()).toEqual(token2.getData().getToken());
    expect(token1.getData().getTokenExpiresOn()).toEqual(token2.getData().getTokenExpiresOn());

    compareUsers(token1.getData().getUser(), token2.getData().getUser());
    compareOauthClients(token1.getData().getClient(), token2.getData().getClient());
}
