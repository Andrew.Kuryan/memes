import Image, { ImageData, ImageFilter } from './Image';
import User from './User';
import Tag from './Tag';
import Template from './Template';
import { differenceWith } from 'lodash';

export class ConstructedMemFilter extends ImageFilter {
    readonly templatesId: Array<number> | undefined;

    constructor({
        authorLogin,
        tagsName,
        creationDateFrom,
        creationDateTo,
        likedUsersLogin,
        templatesId,
    }: {
        authorLogin?: string;
        tagsName?: Array<string>;
        creationDateFrom?: Date;
        creationDateTo?: Date;
        likedUsersLogin?: Array<string>;
        templatesId?: Array<number>;
    }) {
        super({
            authorLogin,
            tagsName,
            creationDateFrom,
            creationDateTo,
            likedUsersLogin,
        });
        this.templatesId = templatesId;
    }

    static EMPTY = new ConstructedMemFilter({});

    match(mem: ConstructedMem): boolean {
        return (
            super.match(mem) &&
            (!this.templatesId ||
                differenceWith(
                    this.templatesId,
                    mem
                        .getData()
                        .getTemplates()
                        .map(t => t.getId()),
                ).length === 0)
        );
    }
}

export class ConstructedMemData extends ImageData {
    private readonly templates: Array<Template>;

    constructor({
        author,
        imageUrl,
        tags,
        likedUsers,
        templates,
    }: {
        author: User;
        imageUrl: string;
        tags: Array<Tag>;
        likedUsers?: Array<User>;
        templates: Array<Template>;
    }) {
        super({ author, imageUrl, tags, likedUsers });
        this.templates = templates;
    }

    getTemplates(): Array<Template> {
        return this.templates;
    }
}

export default class ConstructedMem extends Image {
    constructor({
        id,
        creationDate,
        data,
    }: {
        id: number;
        creationDate: Date;
        data: ConstructedMemData;
    }) {
        super({ id, creationDate, data });
    }

    getData(): ConstructedMemData {
        return super.getData() as ConstructedMemData;
    }
}
