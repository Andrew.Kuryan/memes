import OAuthClient from './OAuthClient';
import User from './User';

export class OAuthTokenData {
    private readonly token: string;
    private readonly tokenExpiresOn: Date;
    private readonly client: OAuthClient;
    private readonly user: User;

    constructor({
        token,
        tokenExpiresOn,
        client,
        user,
    }: {
        token: string;
        tokenExpiresOn: Date;
        client: OAuthClient;
        user: User;
    }) {
        this.token = token;
        this.tokenExpiresOn = tokenExpiresOn;
        this.client = client;
        this.user = user;
    }

    getToken(): string {
        return this.token;
    }

    getTokenExpiresOn(): Date {
        return this.tokenExpiresOn;
    }

    getClient(): OAuthClient {
        return this.client;
    }

    getUser(): User {
        return this.user;
    }
}

export class OAuthTokenFactory {
    create({ id, data }: { id: number; data: OAuthTokenData }): OAuthToken {
        return new OAuthToken({ id, data });
    }
}

export default class OAuthToken {
    private readonly id: number;
    private readonly data: OAuthTokenData;

    constructor({ id, data }: { id: number; data: OAuthTokenData }) {
        this.id = id;
        this.data = data;
    }

    getId(): number {
        return this.id;
    }

    getData(): OAuthTokenData {
        return this.data;
    }
}

export class AccessTokenFactory extends OAuthTokenFactory {
    create({ id, data }: { id: number; data: OAuthTokenData }): AccessToken {
        return new AccessToken({ id, data });
    }
}

export class AccessToken extends OAuthToken {}

export class RefreshTokenFactory extends OAuthTokenFactory {
    create({ id, data }: { id: number; data: OAuthTokenData }): RefreshToken {
        return new RefreshToken({ id, data });
    }
}

export class RefreshToken extends OAuthToken {}
