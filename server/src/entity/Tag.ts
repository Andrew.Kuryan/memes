export default class Tag {
    private readonly name: string;

    constructor(name: string) {
        this.name = name;
    }

    getName() {
        return this.name;
    }

    equals(otherTag: Tag): boolean {
        return this.name === otherTag.getName();
    }

    static equals(tag1: Tag, tag2: Tag): boolean {
        return tag1.equals(tag2);
    }
}
