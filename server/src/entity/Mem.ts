import Image, { ImageData, ImageFactory } from './Image';

export class MemFactory extends ImageFactory {
    create({ id, creationDate, data }: { id: number; creationDate: Date; data: ImageData }): Mem {
        return new Mem({ id, creationDate, data });
    }
}

export default class Mem extends Image {}
