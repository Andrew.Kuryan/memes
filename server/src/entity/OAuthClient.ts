import { GrantType } from '../db/models/OAuthClientGrantType';

export class OAuthClientData {
    private readonly name: string;
    private readonly accessTokenLifetime: number | null;
    private readonly refreshTokenLifetime: number | null;
    private readonly clientSecret: string;
    private readonly redirectUrl: string | null;
    private readonly grantTypes: Array<GrantType>;

    constructor({
        name,
        accessTokenLifetime,
        refreshTokenLifetime,
        clientSecret,
        redirectUrl,
        grantTypes,
    }: {
        name: string;
        accessTokenLifetime?: number;
        refreshTokenLifetime?: number;
        clientSecret: string;
        redirectUrl?: string;
        grantTypes: Array<GrantType>;
    }) {
        this.name = name;
        this.accessTokenLifetime = accessTokenLifetime ?? null;
        this.refreshTokenLifetime = refreshTokenLifetime ?? null;
        this.clientSecret = clientSecret;
        this.redirectUrl = redirectUrl ?? null;
        this.grantTypes = grantTypes;
    }

    getName(): string {
        return this.name;
    }

    getAccessTokenLifetime(): number | null {
        return this.accessTokenLifetime;
    }

    getRefreshTokenLifetime(): number | null {
        return this.refreshTokenLifetime;
    }

    getClientSecret(): string {
        return this.clientSecret;
    }

    getRedirectUrl(): string | null {
        return this.redirectUrl;
    }

    getGrantTypes(): Array<GrantType> {
        return this.grantTypes;
    }
}

export default class OAuthClient {
    private readonly id: number;
    private readonly data: OAuthClientData;

    constructor({ id, data }: { id: number; data: OAuthClientData }) {
        this.id = id;
        this.data = data;
    }

    getId(): number {
        return this.id;
    }

    getData(): OAuthClientData {
        return this.data;
    }
}
