import Image, { ImageData, ImageFactory } from './Image';

export class TemplateFactory extends ImageFactory {
    create({
        id,
        creationDate,
        data,
    }: {
        id: number;
        creationDate: Date;
        data: ImageData;
    }): Template {
        return new Template({ id, creationDate, data });
    }
}

export default class Template extends Image {}
