import Tag from './Tag';

export default class User {
    private readonly login: string;
    private readonly avatarUrl: string | null;
    private readonly password: string;
    private readonly email: string;
    private readonly subscriptions: Array<Tag>;

    constructor({
        login,
        avatarUrl,
        password,
        email,
        subscriptions,
    }: {
        login: string;
        avatarUrl?: string;
        password: string;
        email: string;
        subscriptions: Array<Tag>;
    }) {
        this.login = login;
        this.avatarUrl = avatarUrl ?? null;
        this.password = password;
        this.email = email;
        this.subscriptions = subscriptions;
    }

    getLogin(): string {
        return this.login;
    }

    getAvatarUrl(): string | null {
        return this.avatarUrl;
    }

    getPassword(): string {
        return this.password;
    }

    getEmail(): string {
        return this.email;
    }

    getSubscriptions(): Array<Tag> {
        return this.subscriptions;
    }

    equals(otherUser: User): boolean {
        return this.getLogin() === otherUser.getLogin();
    }

    static equals(user1: User, user2: User): boolean {
        return user1.equals(user2);
    }
}
