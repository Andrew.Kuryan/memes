import { differenceWith } from 'lodash';

import User from './User';
import Tag from './Tag';

export enum ImageSorter {
    Id,
    Likes,
    Date,
}

export class ImageFilter {
    readonly authorLogin: string | undefined;
    readonly tagsName: Array<string> | undefined;
    readonly creationDateFrom: Date | undefined;
    readonly creationDateTo: Date | undefined;
    readonly likedUsersLogin: Array<string> | undefined;

    constructor({
        authorLogin,
        tagsName,
        creationDateFrom,
        creationDateTo,
        likedUsersLogin,
    }: {
        authorLogin?: string;
        tagsName?: Array<string>;
        creationDateFrom?: Date;
        creationDateTo?: Date;
        likedUsersLogin?: Array<string>;
    }) {
        this.authorLogin = authorLogin;
        this.tagsName = tagsName;
        this.creationDateFrom = creationDateFrom;
        this.creationDateTo = creationDateTo;
        this.likedUsersLogin = likedUsersLogin;
    }

    static EMPTY = new ImageFilter({});

    match(image: Image): boolean {
        return (
            (this.authorLogin
                ? this.authorLogin ===
                  image
                      .getData()
                      .getAuthor()
                      .getLogin()
                : true) &&
            (!this.creationDateFrom || this.creationDateFrom < image.getCreationDate()) &&
            (!this.creationDateTo || this.creationDateTo > image.getCreationDate()) &&
            (!this.tagsName ||
                differenceWith(
                    this.tagsName,
                    image
                        .getData()
                        .getTags()
                        .map(t => t.getName()),
                ).length === 0) &&
            (!this.likedUsersLogin ||
                differenceWith(
                    this.likedUsersLogin,
                    image
                        .getData()
                        .getLikedUsers()
                        .map(u => u.getLogin()),
                ).length === 0)
        );
    }
}

export class ImageFactory {
    create({ id, creationDate, data }: { id: number; creationDate: Date; data: ImageData }): Image {
        return new Image({ id, creationDate, data });
    }
}

export class ImageData {
    private readonly author: User;
    private readonly imageUrl: string;
    private readonly tags: Array<Tag>;
    private readonly likedUsers: Array<User>;

    constructor({
        author,
        imageUrl,
        tags,
        likedUsers,
    }: {
        author: User;
        imageUrl: string;
        tags: Array<Tag>;
        likedUsers?: Array<User>;
    }) {
        this.author = author;
        this.imageUrl = imageUrl;
        this.tags = tags;
        this.likedUsers = likedUsers ?? [];
    }

    getAuthor(): User {
        return this.author;
    }

    getImageUrl(): string {
        return this.imageUrl;
    }

    getTags(): Array<Tag> {
        return this.tags;
    }

    getLikedUsers(): Array<User> {
        return this.likedUsers;
    }
}

export default class Image {
    private readonly id: number;
    private readonly creationDate: Date;
    private readonly data: ImageData;

    constructor({ id, creationDate, data }: { id: number; creationDate: Date; data: ImageData }) {
        this.id = id;
        this.creationDate = creationDate ?? new Date();
        this.data = data;
    }

    getId(): number {
        return this.id;
    }

    getCreationDate(): Date {
        return this.creationDate;
    }

    getData(): ImageData {
        return this.data;
    }

    equals(otherImage: Image): boolean {
        return this.getId() === otherImage.getId();
    }

    static equals(image1: Image, image2: Image): boolean {
        return image1.equals(image2);
    }
}
