import ConstructedMem, {
    ConstructedMemData,
    ConstructedMemFilter,
} from '../../entity/ConstructedMem';
import { ArrayResult, Pagination } from './Pagination';
import { ImageSorter } from '../../entity/Image';

export default interface ConstructedMemRepository {
    create(memData: ConstructedMemData): Promise<ConstructedMem>;

    remove(mem: ConstructedMem): Promise<ConstructedMem>;

    getById(id: number): Promise<ConstructedMem>;

    search(
        filter: ConstructedMemFilter,
        pagination: Pagination,
        sorter: ImageSorter,
    ): Promise<ArrayResult<ConstructedMem>>;

    update(mem: ConstructedMem, newData: ConstructedMemData): Promise<ConstructedMem>;
}
