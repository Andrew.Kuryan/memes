import Template from '../../entity/Template';
import ImageRepository from './ImageRepository';

export default interface TemplateRepository extends ImageRepository<Template> {}
