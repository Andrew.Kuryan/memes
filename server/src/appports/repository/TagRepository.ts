import Tag from '../../entity/Tag';
import { ArrayResult, Pagination } from './Pagination';

export default interface TagRepository {
    create(tag: Tag): Promise<Tag>;

    getAll(pagination: Pagination): Promise<ArrayResult<Tag>>;

    search(pagination: Pagination, regexp: RegExp): Promise<ArrayResult<Tag>>;

    remove(tag: Tag): Promise<Tag>;

    exists(name: string): Promise<boolean>;
}
