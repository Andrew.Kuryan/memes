import OAuthClient, { OAuthClientData } from '../../entity/OAuthClient';

export default interface OAuthClientRepository {
    create(clientData: OAuthClientData): Promise<OAuthClient>;

    getById(id: number): Promise<OAuthClient | null>;

    remove(client: OAuthClient): Promise<OAuthClient>;
}
