import OAuthToken, { AccessToken, OAuthTokenData, RefreshToken } from '../../entity/OAuthToken';

export default interface OAuthTokenRepository<T extends OAuthToken> {
    create(tokenData: OAuthTokenData): Promise<T>;

    getByToken(token: string): Promise<T | null>;

    remove(token: T): Promise<T>;
}

export interface AccessTokenRepository extends OAuthTokenRepository<AccessToken> {}

export interface RefreshTokenRepository extends OAuthTokenRepository<RefreshToken> {}
