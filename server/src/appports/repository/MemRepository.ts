import Mem from '../../entity/Mem';
import ImageRepository from './ImageRepository';

export default interface MemRepository extends ImageRepository<Mem> {}
