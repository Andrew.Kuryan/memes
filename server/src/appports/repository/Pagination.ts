export interface Pagination {
    limit: number;
    page: number;
}

export interface ArrayResult<T> {
    result: Array<T>;
    totalCount: number;
}
