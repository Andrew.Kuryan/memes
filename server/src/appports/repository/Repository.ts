import UserRepository from './UserRepository';
import TagRepository from './TagRepository';
import MemRepository from './MemRepository';
import TemplateRepository from './TemplateRepository';
import ConstructedMemRepository from './ConstructedMemRepository';

export default interface Repository {
    readonly userRepository: UserRepository;

    readonly tagRepository: TagRepository;

    readonly memRepository: MemRepository;

    readonly templateRepository: TemplateRepository;

    readonly constructedMemRepository: ConstructedMemRepository;
}
