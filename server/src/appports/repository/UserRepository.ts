import User from '../../entity/User';

export default interface UserRepository {
    create(user: User): Promise<User>;

    remove(user: User): Promise<User>;

    getByLogin(login: string): Promise<User>;

    update(oldUser: User, newUser: User): Promise<User>;
}
