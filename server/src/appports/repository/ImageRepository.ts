import Image, { ImageData, ImageFilter, ImageSorter } from '../../entity/Image';
import { ArrayResult, Pagination } from './Pagination';

export default interface ImageRepository<T extends Image> {
    create(itemData: ImageData): Promise<T>;

    remove(item: T): Promise<T>;

    getById(id: number): Promise<T>;

    search(
        filter: ImageFilter,
        pagination: Pagination,
        sorter: ImageSorter,
    ): Promise<ArrayResult<T>>;

    update(item: T, newData: ImageData): Promise<T>;
}
