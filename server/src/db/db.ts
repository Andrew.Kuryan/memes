import * as fs from 'fs';
import * as path from 'path';
import * as minimist from 'minimist';

import { models, sequelize } from '../dbrepository/config';

const queryInterface = sequelize.getQueryInterface();

const args = minimist(process.argv.slice(2));
const command = args._[0];

switch (command) {
    case 'migrate':
        handlePromise(migrate(), 'Migrating was successful', "Can't execute migrating");
        break;
    case 'revert':
        handlePromise(revert(), 'Reverting was successful', "Can't execute reverting");
        break;
    case 'seeders':
        handlePromise(seeders(), 'Seeders was successfully write', "Can't execute seeders writing");
        break;
    case 'clear':
        handlePromise(clear(), 'Tables was successfully cleared', "Can't clear tables");
        break;
    default:
        console.log(`Usage: npm run db -- [command]
where command: 
    migrate: create tables in database
    revert: drop tables in database
    seeders: write values from json to database
    clear: clear all records in tables`);
}

function handlePromise(promise: Promise<any>, successMessage: string, errorMessage: string) {
    promise
        .then(() => {
            console.log(successMessage);
            process.exit(0);
        })
        .catch(err => {
            console.log(errorMessage);
            console.log(err);
            process.exit(0);
        });
}

async function migrate() {
    for (const model of Object.values(models)) {
        await queryInterface.createTable(model.tableName, model.rawAttributes);
    }
}

async function revert() {
    for (const model of Object.values(models).reverse()) {
        await queryInterface.dropTable(model.tableName);
    }
}

async function seeders() {
    const files = await readSeeders();
    const fileNamesWithoutExts = files.map(file => file.substring(0, file.indexOf('.json')));
    for (const model of Object.values(models)) {
        const filePos = fileNamesWithoutExts.indexOf(model.tableName);
        if (filePos !== -1) {
            const objects = await readObjects(files[filePos]);
            // @ts-ignore
            await model.bulkCreate(objects);
        }
    }
}

async function clear() {
    for (const model of Object.values(models).reverse()) {
        await model.truncate({
            cascade: true,
        });
    }
}

async function readObjects(fileName: string): Promise<Array<object>> {
    return new Promise<Array<object>>((resolve, reject) => {
        fs.readFile(path.resolve(__dirname, 'seeders', fileName), (err, data) => {
            if (err === null) {
                try {
                    const objects = JSON.parse(data.toString());
                    resolve(objects);
                } catch (err) {
                    reject(err);
                }
            } else {
                reject(err);
            }
        });
    });
}

async function readSeeders(): Promise<Array<string>> {
    return new Promise<Array<string>>((resolve, reject) => {
        fs.readdir(path.resolve(__dirname, 'seeders'), async (err, files) => {
            if (err === null) {
                resolve(files);
            } else {
                reject(err);
            }
        });
    });
}
