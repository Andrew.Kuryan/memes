import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    BelongsToMany,
    Column,
    CreatedAt,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';
import DBUser from './DBUser';
import DBTag from './DBTag';
import DBImage from './DBImage';
import ConstructedTemplate from './ConstructedTemplate';
import ConstructedMemTag from './ConstructedMemTag';
import ConstructedMemLike from './ConstructedMemLike';

@Table({ timestamps: true, updatedAt: false, tableName: 'constructed_memes' })
export default class DBConstructedMem extends Model<DBConstructedMem> {
    @AllowNull(false)
    @PrimaryKey
    @Unique
    @AutoIncrement
    @Column
    id: number;

    @CreatedAt
    creationDate: Date;

    @BelongsTo(() => DBUser, 'authorLogin')
    author: DBUser;

    @AllowNull(false)
    @Column
    imageUrl: string;

    @BelongsToMany(
        () => DBTag,
        () => ConstructedMemTag,
    )
    tags: Array<DBTag>;

    @BelongsToMany(
        () => DBUser,
        () => ConstructedMemLike,
    )
    likedUsers: Array<DBUser>;

    @BelongsToMany(
        () => DBImage,
        () => ConstructedTemplate,
    )
    templates: Array<DBImage>;
}
