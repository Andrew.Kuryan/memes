import {
    AllowNull,
    BelongsToMany,
    Column,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';

import DBTag from './DBTag';
import UserTag from './UserTag';
import DBOAuthToken from './DBOAuthToken';

@Table({ timestamps: false, tableName: 'users' })
export default class DBUser extends Model<DBUser> {
    @AllowNull(false)
    @Unique
    @PrimaryKey
    @Column
    login: string;

    @AllowNull(true)
    @Column
    avatarUrl: string;

    @AllowNull(false)
    @Column
    password: string;

    @AllowNull(false)
    @Column
    email: string;

    @BelongsToMany(
        () => DBTag,
        () => UserTag,
    )
    subscriptions: Array<DBTag>;
}
