import { AllowNull, Column, Model, PrimaryKey, Table, Unique } from 'sequelize-typescript';

@Table({ timestamps: false, tableName: 'tags' })
export default class DBTag extends Model<DBTag> {
    @AllowNull(false)
    @PrimaryKey
    @Unique
    @Column
    name: string;
}
