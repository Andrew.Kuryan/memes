import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    Column,
    HasOne,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';
import DBUser from './DBUser';
import DBOAuthClient from './DBOAuthClient';

export enum TokenType {
    Access,
    Refresh,
}

@Table({ timestamps: false, tableName: 'oauth_tokens' })
export default class DBOAuthToken extends Model<DBOAuthToken> {
    @AllowNull(false)
    @Unique
    @AutoIncrement
    @PrimaryKey
    @Column
    id: number;

    @AllowNull(false)
    @Column
    token: string;

    @AllowNull(false)
    @Column
    tokenExpiresOn: Date;

    @AllowNull(false)
    @Column
    type: TokenType;

    @BelongsTo(() => DBOAuthClient, 'oauthClientId')
    client: DBOAuthClient;

    @BelongsTo(() => DBUser, 'userLogin')
    user: DBUser;
}
