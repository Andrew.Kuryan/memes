import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import DBConstructedMem from './DBConstructedMem';
import DBTag from './DBTag';

@Table({ timestamps: false, tableName: 'constructed_mem_tags' })
export default class ConstructedMemTag extends Model<ConstructedMemTag> {
    @AllowNull(false)
    @ForeignKey(() => DBConstructedMem)
    @Column
    constructedMemId: number;

    @AllowNull(false)
    @ForeignKey(() => DBTag)
    @Column
    tagName: string;
}
