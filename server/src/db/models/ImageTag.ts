import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';

import DBImage from './DBImage';
import DBTag from './DBTag';

@Table({ timestamps: false, tableName: 'image_tags' })
export default class ImageTag extends Model<ImageTag> {
    @AllowNull(false)
    @ForeignKey(() => DBImage)
    @Column
    imageId: number;

    @AllowNull(false)
    @ForeignKey(() => DBTag)
    @Column
    tagName: string;
}
