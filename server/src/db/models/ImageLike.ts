import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';

import DBImage from './DBImage';
import DBUser from './DBUser';

@Table({ timestamps: false, tableName: 'image_likes' })
export default class ImageLike extends Model<ImageLike> {
    @AllowNull(false)
    @ForeignKey(() => DBImage)
    @Column
    imageId: number;

    @AllowNull(false)
    @ForeignKey(() => DBUser)
    @Column
    userLogin: string;
}
