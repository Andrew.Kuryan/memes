import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';

import DBImage from './DBImage';
import DBConstructedMem from './DBConstructedMem';

@Table({ timestamps: false, tableName: 'constructed_templates' })
export default class ConstructedTemplate extends Model<ConstructedTemplate> {
    @AllowNull(false)
    @ForeignKey(() => DBConstructedMem)
    @Column
    constructedMemId: number;

    @AllowNull(false)
    @ForeignKey(() => DBImage)
    @Column
    templateId: number;
}
