import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import DBConstructedMem from './DBConstructedMem';
import DBUser from './DBUser';

@Table({ timestamps: false, tableName: 'constructed_mem_likes' })
export default class ConstructedMemLike extends Model<ConstructedMemLike> {
    @AllowNull(false)
    @ForeignKey(() => DBConstructedMem)
    @Column
    constructedMemId: number;

    @AllowNull(false)
    @ForeignKey(() => DBUser)
    @Column
    userLogin: string;
}
