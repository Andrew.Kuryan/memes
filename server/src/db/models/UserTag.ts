import { AllowNull, Column, ForeignKey, Model, Table } from 'sequelize-typescript';

import DBUser from './DBUser';
import DBTag from './DBTag';

@Table({ timestamps: false, tableName: 'user_tags' })
export default class UserTag extends Model<UserTag> {
    @AllowNull(false)
    @ForeignKey(() => DBUser)
    @Column
    userLogin: string;

    @AllowNull(false)
    @ForeignKey(() => DBTag)
    @Column
    tagName: string;
}
