import {
    AllowNull,
    AutoIncrement,
    Column,
    ForeignKey,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';
import DBOAuthClient from './DBOAuthClient';

export type GrantType = 'password' | 'refresh_token' | 'authorization_code' | 'client_credentials';

@Table({ timestamps: false, tableName: 'oauth_client_grant_types' })
export default class OAuthClientGrantType extends Model<OAuthClientGrantType> {
    @AllowNull(false)
    @PrimaryKey
    @Unique
    @AutoIncrement
    @Column
    id: number;

    @AllowNull(false)
    @Column
    typeName: GrantType;

    @AllowNull(false)
    @ForeignKey(() => DBOAuthClient)
    @Column
    oauthClientId: number;
}
