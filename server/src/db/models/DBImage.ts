import {
    AllowNull,
    AutoIncrement,
    BelongsTo,
    BelongsToMany,
    Column,
    CreatedAt,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';

import DBTag from './DBTag';
import DBUser from './DBUser';
import ImageTag from './ImageTag';
import ImageLike from './ImageLike';

export enum ImageType {
    Mem,
    Template,
}

@Table({ timestamps: true, updatedAt: false, tableName: 'images' })
export default class DBImage extends Model<DBImage> {
    @AllowNull(false)
    @PrimaryKey
    @Unique
    @AutoIncrement
    @Column
    id: number;

    @CreatedAt
    creationDate: Date;

    @BelongsTo(() => DBUser, 'authorLogin')
    author: DBUser;

    @AllowNull(false)
    @Column
    imageUrl: string;

    @BelongsToMany(
        () => DBTag,
        () => ImageTag,
    )
    tags: Array<DBTag>;

    @BelongsToMany(
        () => DBUser,
        () => ImageLike,
    )
    likedUsers: Array<DBUser>;

    @AllowNull(false)
    @Column
    type: ImageType;
}
