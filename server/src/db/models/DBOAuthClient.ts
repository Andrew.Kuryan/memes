import {
    AllowNull,
    AutoIncrement,
    Column,
    Default,
    ForeignKey,
    HasMany,
    Model,
    PrimaryKey,
    Table,
    Unique,
} from 'sequelize-typescript';
import OAuthClientGrantType from './OAuthClientGrantType';
import DBOAuthToken from './DBOAuthToken';

@Table({ timestamps: false, tableName: 'oauth_clients' })
export default class DBOAuthClient extends Model<DBOAuthClient> {
    @AllowNull(false)
    @Unique
    @AutoIncrement
    @PrimaryKey
    @Column
    id: number;

    @AllowNull(false)
    @Column
    name: string;

    @AllowNull(false)
    @Default(14400) // 4 hours
    @Column
    accessTokenLifetime: number;

    @AllowNull(false)
    @Default(86400) // 1 day
    @Column
    refreshTokenLifetime: number;

    @AllowNull(false)
    @Column
    clientSecret: string;

    @AllowNull(true)
    @Column
    redirectUrl: string;

    @HasMany(() => OAuthClientGrantType)
    grantTypes: Array<OAuthClientGrantType>;
}
