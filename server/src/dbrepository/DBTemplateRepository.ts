import DBImageRepository from './DBImageRepository';
import Template, { TemplateFactory } from '../entity/Template';
import { ImageType } from '../db/models/DBImage';
import { parseTemplate } from './adapters/template';

export default class DBTemplateRepository extends DBImageRepository<Template> {
    constructor() {
        super(ImageType.Template, new TemplateFactory(), parseTemplate);
    }
}
