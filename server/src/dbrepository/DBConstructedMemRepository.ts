import { identity, sortBy } from 'lodash';
import { FindOptions, Op } from 'sequelize';

import ConstructedMem, { ConstructedMemData, ConstructedMemFilter } from '../entity/ConstructedMem';
import Image, { ImageSorter } from '../entity/Image';
import ConstructedMemRepository from '../appports/repository/ConstructedMemRepository';
import { models } from './config';
import parseConstructedMem from './adapters/constructedMem';
import { listDifference, updateChildTable, whereDateRange, whereIncludesInArray } from './utils';
import Tag from '../entity/Tag';
import User from '../entity/User';
import Template from '../entity/Template';
import { imageInclude } from './DBImageRepository';
import { userInclude } from './DBUserRepository';
import ApiError from '../appports/repository/ApiError';
import { ArrayResult, Pagination } from '../appports/repository/Pagination';

export const constructedMemInclude = [
    ...imageInclude,
    {
        model: models.DBImage,
        include: imageInclude,
    },
];

export default class DBConstructedMemRepository implements ConstructedMemRepository {
    async create(memData: ConstructedMemData): Promise<ConstructedMem> {
        const result = await models.DBConstructedMem.build({
            authorLogin: memData.getAuthor().getLogin(),
            imageUrl: memData.getImageUrl(),
        }).save();

        await DBConstructedMemRepository.createConstructedMemTags(result.id, memData.getTags());
        await DBConstructedMemRepository.createConstructedMemLikes(
            result.id,
            memData.getLikedUsers(),
        );
        await DBConstructedMemRepository.createConstructedTemplates(
            result.id,
            memData.getTemplates(),
        );

        return Promise.resolve(
            new ConstructedMem({
                id: result.id,
                creationDate: result.creationDate,
                data: memData,
            }),
        );
    }

    async getById(id: number): Promise<ConstructedMem> {
        const dbConstructedMem = await models.DBConstructedMem.findByPk(id, {
            include: constructedMemInclude,
        });
        if (dbConstructedMem != null) {
            return Promise.resolve(parseConstructedMem(dbConstructedMem));
        } else {
            return Promise.reject(new ApiError('No such constructed mem'));
        }
    }

    async remove(mem: ConstructedMem): Promise<ConstructedMem> {
        await models.ConstructedMemLike.destroy({
            where: {
                constructedMemId: mem.getId(),
            },
        });
        await models.ConstructedMemTag.destroy({
            where: {
                constructedMemId: mem.getId(),
            },
        });
        await models.ConstructedTemplate.destroy({
            where: {
                constructedMemId: mem.getId(),
            },
        });
        const dbConstructedMem = await models.DBConstructedMem.findByPk(mem.getId());
        await dbConstructedMem?.destroy();
        return Promise.resolve(mem);
    }

    async search(
        filter: ConstructedMemFilter,
        pagination: Pagination,
        sorter: ImageSorter = ImageSorter.Date,
    ): Promise<ArrayResult<ConstructedMem>> {
        const { rows: result, count } = await models.DBConstructedMem.findAndCountAll({
            ...DBConstructedMemRepository.filterToFindOptions(filter),
            order: [['creationDate', 'DESC']],
            offset: (pagination.page - 1) * pagination.limit,
            limit: pagination.limit,
            distinct: true,
        });
        return Promise.resolve({
            result: sortBy(result.map(parseConstructedMem), this.sorterToComparator(sorter)),
            totalCount: count,
        });
    }

    async update(mem: ConstructedMem, newData: ConstructedMemData): Promise<ConstructedMem> {
        await models.DBConstructedMem.update(
            {
                authorLogin: newData.getAuthor().getLogin(),
                imageUrl: newData.getImageUrl(),
            },
            {
                where: {
                    id: mem.getId(),
                },
            },
        );

        await updateChildTable({
            ...listDifference(mem.getData().getTags(), newData.getTags(), Tag.equals),
            parentPkey: { name: 'constructedMemId', value: mem.getId() },
            childPkey: { name: 'tagName', extractor: t => t.getName() },
            destroy: models.ConstructedMemTag.destroy.bind(models.ConstructedMemTag),
            createChildren: DBConstructedMemRepository.createConstructedMemTags,
        });

        await updateChildTable({
            ...listDifference(mem.getData().getLikedUsers(), newData.getLikedUsers(), User.equals),
            parentPkey: { name: 'constructedMemId', value: mem.getId() },
            childPkey: { name: 'userLogin', extractor: l => l.getLogin() },
            destroy: models.ConstructedMemLike.destroy.bind(models.ConstructedMemLike),
            createChildren: DBConstructedMemRepository.createConstructedMemLikes,
        });

        await updateChildTable({
            ...listDifference(mem.getData().getTemplates(), newData.getTemplates(), Image.equals),
            parentPkey: { name: 'constructedMemId', value: mem.getId() },
            childPkey: { name: 'templateId', extractor: l => l.getId() },
            destroy: models.ConstructedTemplate.destroy.bind(models.ConstructedTemplate),
            createChildren: DBConstructedMemRepository.createConstructedTemplates,
        });

        return Promise.resolve(
            new ConstructedMem({
                id: mem.getId(),
                creationDate: mem.getCreationDate(),
                data: newData,
            }),
        );
    }

    private sorterToComparator(sorter: ImageSorter): (image: ConstructedMem) => number {
        if (sorter === ImageSorter.Id) {
            return image => image.getId();
        } else if (sorter === ImageSorter.Date) {
            return image => 0 - image.getCreationDate().getTime();
        } else if (sorter === ImageSorter.Likes) {
            return image => 0 - image.getData().getLikedUsers().length;
        } else {
            return image => image.getId();
        }
    }

    private static filterToFindOptions(filter: ConstructedMemFilter): FindOptions {
        return {
            where: {
                [Op.and]: [
                    ...(filter.authorLogin ? [{ authorLogin: filter.authorLogin }] : []),
                    {
                        creationDate: whereDateRange(
                            filter.creationDateFrom,
                            filter.creationDateTo,
                        ),
                    },
                ],
            },
            include: [
                { model: models.DBUser, as: 'author', include: userInclude },
                {
                    model: models.DBTag,
                    ...whereIncludesInArray({
                        pkey: { name: 'name', extractor: identity },
                        inArr: filter.tagsName,
                    }),
                },
                {
                    model: models.DBUser,
                    as: 'likedUsers',
                    include: userInclude,
                    ...whereIncludesInArray({
                        pkey: { name: 'login', extractor: identity },
                        inArr: filter.likedUsersLogin,
                    }),
                },
                {
                    model: models.DBImage,
                    include: imageInclude,
                    ...whereIncludesInArray({
                        pkey: { name: 'id', extractor: identity },
                        inArr: filter.templatesId,
                    }),
                },
            ],
        };
    }

    private static async createConstructedMemTags(constructedMemId: number, tags: Array<Tag>) {
        for (const tag of tags) {
            await models.ConstructedMemTag.build({
                constructedMemId,
                tagName: tag.getName(),
            }).save();
        }
    }

    private static async createConstructedMemLikes(constructedMemId: number, users: Array<User>) {
        for (const like of users) {
            await models.ConstructedMemLike.build({
                constructedMemId,
                userLogin: like.getLogin(),
            }).save();
        }
    }

    private static async createConstructedTemplates(
        constructedMemId: number,
        templates: Array<Template>,
    ) {
        for (const template of templates) {
            await models.ConstructedTemplate.build({
                constructedMemId,
                templateId: template.getId(),
            }).save();
        }
    }
}
