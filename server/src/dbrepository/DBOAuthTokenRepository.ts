import OAuthTokenRepository from '../appports/repository/OAuthTokenRepository';
import OAuthToken, {
    AccessToken,
    AccessTokenFactory,
    OAuthTokenData,
    OAuthTokenFactory,
    RefreshToken,
    RefreshTokenFactory,
} from '../entity/OAuthToken';
import { models } from './config';
import DBOAuthToken, { TokenType } from '../db/models/DBOAuthToken';
import { Op } from 'sequelize';
import { parseAccessToken, parseRefreshToken } from './adapters/oauthToken';
import { userInclude } from './DBUserRepository';
import { oauthClientInclude } from './DBOAuthClientRepository';
import DBOAuthClient from '../db/models/DBOAuthClient';
import DBUser from '../db/models/DBUser';

class DBOAuthTokenRepository<T extends OAuthToken> implements OAuthTokenRepository<T> {
    private readonly type: TokenType;
    private readonly factory: OAuthTokenFactory;
    private readonly parser: (dbOAuthToken: DBOAuthToken) => T;

    constructor(
        type: TokenType,
        factory: OAuthTokenFactory,
        parser: (dbOAuthToken: DBOAuthToken) => T,
    ) {
        this.type = type;
        this.factory = factory;
        this.parser = parser;
    }

    async create(tokenData: OAuthTokenData): Promise<T> {
        const result = await models.DBOAuthToken.build({
            token: tokenData.getToken(),
            tokenExpiresOn: tokenData.getTokenExpiresOn(),
            userLogin: tokenData.getUser().getLogin(),
            oauthClientId: tokenData.getClient().getId(),
            type: this.type,
        }).save();
        return Promise.resolve(this.factory.create({ id: result.id, data: tokenData }) as T);
    }

    async getByToken(token: string): Promise<T | null> {
        const dbOAuthToken = await models.DBOAuthToken.findOne({
            where: {
                [Op.and]: [
                    {
                        token: token,
                    },
                    {
                        type: this.type,
                    },
                ],
            },
            include: [
                { model: DBOAuthClient, as: 'client', include: oauthClientInclude },
                { model: DBUser, as: 'user', include: userInclude },
            ],
        });
        if (dbOAuthToken != null) {
            return Promise.resolve(this.parser(dbOAuthToken));
        } else {
            return Promise.resolve(null);
        }
    }

    async remove(token: T): Promise<T> {
        const dbOAuthToken = await models.DBOAuthToken.findByPk(token.getId());
        await dbOAuthToken?.destroy();
        return Promise.resolve(token);
    }
}

export class DBAccessTokenRepository extends DBOAuthTokenRepository<AccessToken> {
    constructor() {
        super(TokenType.Access, new AccessTokenFactory(), parseAccessToken);
    }
}

export class DBRefreshTokenRepository extends DBOAuthTokenRepository<RefreshToken> {
    constructor() {
        super(TokenType.Refresh, new RefreshTokenFactory(), parseRefreshToken);
    }
}
