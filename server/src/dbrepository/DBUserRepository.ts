import UserRepository from '../appports/repository/UserRepository';
import User from '../entity/User';
import sha256 = require('sha256');

import { models } from './config';
import Tag from '../entity/Tag';
import { parseUser } from './adapters/user';
import { listDifference, updateChildTable } from './utils';
import ApiError from '../appports/repository/ApiError';

export const userInclude = [models.DBTag];

export default class DBUserRepository implements UserRepository {
    async create(user: User): Promise<User> {
        await models.DBUser.build({
            login: user.getLogin(),
            avatarUrl: user.getAvatarUrl(),
            password: sha256(`${user.getLogin()}:${user.getPassword()}`),
            email: user.getEmail(),
            subscriptions: user.getSubscriptions(),
        }).save();
        await DBUserRepository.createSubscriptions(user.getLogin(), user.getSubscriptions());
        return Promise.resolve(user);
    }

    async getByLogin(login: string): Promise<User> {
        const dbUser = await models.DBUser.findByPk(login, { include: userInclude });
        if (dbUser != null) {
            return Promise.resolve(parseUser(dbUser));
        } else {
            return Promise.reject(new ApiError('No such user'));
        }
    }

    async remove(user: User): Promise<User> {
        await models.UserTag.destroy({
            where: {
                userLogin: user.getLogin(),
            },
        });
        const dbUser = await models.DBUser.findByPk(user.getLogin());
        await dbUser?.destroy();
        return Promise.resolve(user);
    }

    async update(oldUser: User, newUser: User): Promise<User> {
        await models.DBUser.update(
            {
                avatarUrl: newUser.getAvatarUrl(),
                password: newUser.getPassword(),
                email: newUser.getEmail(),
            },
            {
                where: { login: newUser.getLogin() },
            },
        );

        await updateChildTable({
            ...listDifference(oldUser.getSubscriptions(), newUser.getSubscriptions(), Tag.equals),
            parentPkey: { name: 'userLogin', value: oldUser.getLogin() },
            childPkey: { name: 'tagName', extractor: s => s.getName() },
            destroy: models.UserTag.destroy.bind(models.UserTag),
            createChildren: DBUserRepository.createSubscriptions,
        });

        return Promise.resolve(newUser);
    }

    private static async createSubscriptions(userLogin: string, subscriptions: Array<Tag>) {
        for (const subscription of subscriptions) {
            await models.UserTag.build({
                userLogin: userLogin,
                tagName: subscription.getName(),
            }).save();
        }
    }
}
