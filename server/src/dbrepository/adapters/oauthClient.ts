import DBOAuthClient from '../../db/models/DBOAuthClient';
import OAuthClient, { OAuthClientData } from '../../entity/OAuthClient';
import OAuthClientGrantType, { GrantType } from '../../db/models/OAuthClientGrantType';

function parseGrantType(dbOAuthClientGrantType: OAuthClientGrantType): GrantType {
    return dbOAuthClientGrantType.typeName;
}

export function parseOauthClient(dbOAuthClient: DBOAuthClient): OAuthClient {
    return new OAuthClient({
        id: dbOAuthClient.id,
        data: new OAuthClientData({
            name: dbOAuthClient.name,
            accessTokenLifetime: dbOAuthClient.accessTokenLifetime,
            refreshTokenLifetime: dbOAuthClient.refreshTokenLifetime,
            clientSecret: dbOAuthClient.clientSecret,
            redirectUrl: dbOAuthClient.redirectUrl,
            grantTypes: dbOAuthClient.grantTypes.map(parseGrantType),
        }),
    });
}
