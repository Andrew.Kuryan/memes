import { map } from 'lodash';

import User from '../../entity/User';
import DBUser from '../../db/models/DBUser';
import { parseTag } from './tag';

export function parseUser(dbUser: DBUser): User {
    return new User({
        login: dbUser.login,
        avatarUrl: dbUser.avatarUrl,
        password: dbUser.password,
        email: dbUser.email,
        subscriptions: map(dbUser.subscriptions, parseTag),
    });
}
