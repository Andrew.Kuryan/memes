import DBOAuthToken from '../../db/models/DBOAuthToken';
import { AccessToken, OAuthTokenData, RefreshToken } from '../../entity/OAuthToken';
import { parseOauthClient } from './oauthClient';
import { parseUser } from './user';

export function parseAccessToken(dbOAuthToken: DBOAuthToken): AccessToken {
    return new AccessToken({
        id: dbOAuthToken.id,
        data: new OAuthTokenData({
            token: dbOAuthToken.token,
            tokenExpiresOn: dbOAuthToken.tokenExpiresOn,
            client: parseOauthClient(dbOAuthToken.client),
            user: parseUser(dbOAuthToken.user),
        }),
    });
}

export function parseRefreshToken(dbOAuthToken: DBOAuthToken): RefreshToken {
    return new RefreshToken({
        id: dbOAuthToken.id,
        data: new OAuthTokenData({
            token: dbOAuthToken.token,
            tokenExpiresOn: dbOAuthToken.tokenExpiresOn,
            client: parseOauthClient(dbOAuthToken.client),
            user: parseUser(dbOAuthToken.user),
        }),
    });
}
