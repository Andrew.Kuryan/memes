import { map } from 'lodash';

import Mem from '../../entity/Mem';
import DBImage from '../../db/models/DBImage';
import { ImageData } from '../../entity/Image';
import { parseUser } from './user';
import { parseTag } from './tag';

export function parseMem(dbMem: DBImage): Mem {
    return new Mem({
        id: dbMem.id,
        creationDate: dbMem.creationDate,
        data: new ImageData({
            author: parseUser(dbMem.author),
            imageUrl: dbMem.imageUrl,
            tags: map(dbMem.tags, parseTag),
            likedUsers: map(dbMem.likedUsers, parseUser),
        }),
    });
}
