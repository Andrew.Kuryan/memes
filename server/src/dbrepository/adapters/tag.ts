import DBTag from '../../db/models/DBTag';
import Tag from '../../entity/Tag';

export function parseTag(dbTag: DBTag): Tag {
    return new Tag(dbTag.name);
}
