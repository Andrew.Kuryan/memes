import { map } from 'lodash';

import Template from '../../entity/Template';
import DBImage from '../../db/models/DBImage';
import { ImageData } from '../../entity/Image';
import { parseUser } from './user';
import { parseTag } from './tag';

export function parseTemplate(dbTemplate: DBImage): Template {
    return new Template({
        id: dbTemplate.id,
        creationDate: dbTemplate.creationDate,
        data: new ImageData({
            author: parseUser(dbTemplate.author),
            imageUrl: dbTemplate.imageUrl,
            tags: map(dbTemplate.tags, parseTag),
            likedUsers: map(dbTemplate.likedUsers, parseUser),
        }),
    });
}

/*
const r = parseTemplate({
    id: 1,
    creationDate: new Date(),
    // @ts-ignore
    author: {
        login: 'test',
        // @ts-ignore
        avatarUrl: null,
        // @ts-ignore
        password: 123,
        email: 'test@test.com',
        // @ts-ignore
        subscriptions: []
    },
    imageUrl: 'test-url',
    // @ts-ignore
    tags: [],
    // @ts-ignore
    likedUsers: []
});

console.log(r);
*/
