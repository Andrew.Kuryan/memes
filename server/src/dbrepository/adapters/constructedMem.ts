import { map } from 'lodash';

import DBConstructedMem from '../../db/models/DBConstructedMem';
import ConstructedMem, { ConstructedMemData } from '../../entity/ConstructedMem';
import { parseUser } from './user';
import { parseTag } from './tag';
import { parseTemplate } from './template';

export default function parseConstructedMem(dbMem: DBConstructedMem) {
    return new ConstructedMem({
        id: dbMem.id,
        creationDate: dbMem.creationDate,
        data: new ConstructedMemData({
            author: parseUser(dbMem.author),
            imageUrl: dbMem.imageUrl,
            tags: map(dbMem.tags, parseTag),
            likedUsers: map(dbMem.likedUsers, parseUser),
            templates: map(dbMem.templates, parseTemplate),
        }),
    });
}
