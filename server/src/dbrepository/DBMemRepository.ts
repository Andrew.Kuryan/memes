import Mem, { MemFactory } from '../entity/Mem';
import { ImageType } from '../db/models/DBImage';
import DBImageRepository from './DBImageRepository';
import { parseMem } from './adapters/mem';

export default class DBMemRepository extends DBImageRepository<Mem> {
    constructor() {
        super(ImageType.Mem, new MemFactory(), parseMem);
    }
}
