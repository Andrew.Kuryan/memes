import TagRepository from '../appports/repository/TagRepository';
import Tag from '../entity/Tag';

import { models } from './config';
import { ArrayResult, Pagination } from '../appports/repository/Pagination';
import { parseTag } from './adapters/tag';
import { Op } from 'sequelize';

export default class DBTagRepository implements TagRepository {
    async create(tag: Tag): Promise<Tag> {
        await models.DBTag.build({ name: tag.getName() }).save();
        return Promise.resolve(tag);
    }

    async getAll(pagination: Pagination): Promise<ArrayResult<Tag>> {
        const { rows: result, count } = await models.DBTag.findAndCountAll({
            order: ['name'],
            offset: (pagination.page - 1) * pagination.limit,
            limit: pagination.limit,
        });
        return Promise.resolve({ result: result.map(parseTag), totalCount: count });
    }

    async search(pagination: Pagination, regexp: RegExp): Promise<ArrayResult<Tag>> {
        const { rows: result, count } = await models.DBTag.findAndCountAll({
            order: ['name'],
            offset: (pagination.page - 1) * pagination.limit,
            limit: pagination.limit,
            where: {
                name: {
                    [Op.regexp]: regexp.source,
                },
            },
        });
        return Promise.resolve({ result: result.map(parseTag), totalCount: count });
    }

    async exists(name: string): Promise<boolean> {
        const dbTag = await models.DBTag.findByPk(name);
        return Promise.resolve(dbTag !== null);
    }

    async remove(tag: Tag): Promise<Tag> {
        const dbTag = await models.DBTag.findByPk(tag.getName());
        await dbTag?.destroy();
        return Promise.resolve(tag);
    }
}
