import { identity, sortBy } from 'lodash';
import { FindOptions, Op } from 'sequelize';

import ImageRepository from '../appports/repository/ImageRepository';
import Image, { ImageData, ImageFactory, ImageFilter, ImageSorter } from '../entity/Image';
import { models } from './config';
import DBImage, { ImageType } from '../db/models/DBImage';
import Tag from '../entity/Tag';
import User from '../entity/User';
import { listDifference, updateChildTable, whereDateRange, whereIncludesInArray } from './utils';
import { userInclude } from './DBUserRepository';
import ApiError from '../appports/repository/ApiError';
import { ArrayResult, Pagination } from '../appports/repository/Pagination';

export const imageInclude = [
    { model: models.DBUser, as: 'author', include: userInclude },
    models.DBTag,
    { model: models.DBUser, as: 'likedUsers', include: userInclude },
];

export default class DBImageRepository<T extends Image> implements ImageRepository<T> {
    private readonly type: ImageType;
    private readonly factory: ImageFactory;
    private readonly parser: (dbImage: DBImage) => T;

    constructor(type: ImageType, factory: ImageFactory, parser: (dbImage: DBImage) => T) {
        this.type = type;
        this.factory = factory;
        this.parser = parser;
    }

    async create(imageData: ImageData): Promise<T> {
        const result = await models.DBImage.build({
            authorLogin: imageData.getAuthor().getLogin(),
            imageUrl: imageData.getImageUrl(),
            type: this.type,
        }).save();
        await DBImageRepository.createImageTags(result.id, imageData.getTags());
        await DBImageRepository.createImageLikes(result.id, imageData.getLikedUsers());
        return Promise.resolve(
            this.factory.create({
                id: result.id,
                creationDate: result.creationDate,
                data: imageData,
            }) as T,
        );
    }

    async getById(id: number): Promise<T> {
        const dbImage = await models.DBImage.findOne({
            where: { id, type: this.type },
            include: imageInclude,
        });
        if (dbImage != null) {
            return Promise.resolve(this.parser(dbImage));
        } else {
            return Promise.reject(new ApiError('No such image'));
        }
    }

    async remove(image: T): Promise<T> {
        await models.ImageLike.destroy({
            where: {
                imageId: image.getId(),
            },
        });
        await models.ImageTag.destroy({
            where: {
                imageId: image.getId(),
            },
        });
        const dbImage = await models.DBImage.findByPk(image.getId());
        await dbImage?.destroy();
        return Promise.resolve(image);
    }

    async search(
        filter: ImageFilter,
        pagination: Pagination,
        sorter: ImageSorter = ImageSorter.Date,
    ): Promise<ArrayResult<T>> {
        const { rows: result, count } = await models.DBImage.findAndCountAll({
            ...this.filterToFindOptions(filter),
            order: [['creationDate', 'DESC']],
            offset: (pagination.page - 1) * pagination.limit,
            limit: pagination.limit,
            distinct: true,
        });

        return Promise.resolve({
            result: sortBy(result.map(this.parser), this.sorterToComparator(sorter)),
            totalCount: count,
        });
    }

    async update(image: T, newData: ImageData): Promise<T> {
        await models.DBImage.update(
            {
                authorLogin: newData.getAuthor().getLogin(),
                imageUrl: newData.getImageUrl(),
            },
            {
                where: {
                    id: image.getId(),
                },
            },
        );

        await updateChildTable({
            ...listDifference(image.getData().getTags(), newData.getTags(), Tag.equals),
            parentPkey: { name: 'imageId', value: image.getId() },
            childPkey: { name: 'tagName', extractor: t => t.getName() },
            destroy: models.ImageTag.destroy.bind(models.ImageTag),
            createChildren: DBImageRepository.createImageTags,
        });

        await updateChildTable({
            ...listDifference(
                image.getData().getLikedUsers(),
                newData.getLikedUsers(),
                User.equals,
            ),
            parentPkey: { name: 'imageId', value: image.getId() },
            childPkey: { name: 'userLogin', extractor: l => l.getLogin() },
            destroy: models.ImageLike.destroy.bind(models.ImageLike),
            createChildren: DBImageRepository.createImageLikes,
        });

        return Promise.resolve(
            this.factory.create({
                id: image.getId(),
                creationDate: image.getCreationDate(),
                data: newData,
            }) as T,
        );
    }

    private sorterToComparator(sorter: ImageSorter): (image: Image) => number {
        if (sorter === ImageSorter.Id) {
            return image => image.getId();
        } else if (sorter === ImageSorter.Date) {
            return image => 0 - image.getCreationDate().getTime();
        } else if (sorter === ImageSorter.Likes) {
            return image => 0 - image.getData().getLikedUsers().length;
        } else {
            return image => image.getId();
        }
    }

    private filterToFindOptions(filter: ImageFilter): FindOptions {
        return {
            where: {
                [Op.and]: [
                    { type: this.type },
                    ...(filter.authorLogin ? [{ authorLogin: filter.authorLogin }] : []),
                    {
                        creationDate: whereDateRange(
                            filter.creationDateFrom,
                            filter.creationDateTo,
                        ),
                    },
                ],
            },
            include: [
                { model: models.DBUser, as: 'author', include: userInclude },
                {
                    model: models.DBTag,
                    ...whereIncludesInArray({
                        pkey: { name: 'name', extractor: identity },
                        inArr: filter.tagsName,
                    }),
                },
                {
                    model: models.DBUser,
                    as: 'likedUsers',
                    include: userInclude,
                    ...whereIncludesInArray({
                        pkey: { name: 'login', extractor: identity },
                        inArr: filter.likedUsersLogin,
                    }),
                },
            ],
        };
    }

    private static async createImageTags(imageId: number, tags: Array<Tag>) {
        for (const tag of tags) {
            await models.ImageTag.build({
                imageId,
                tagName: tag.getName(),
            }).save();
        }
    }

    private static async createImageLikes(imageId: number, users: Array<User>) {
        for (const like of users) {
            await models.ImageLike.build({
                imageId,
                userLogin: like.getLogin(),
            }).save();
        }
    }
}
