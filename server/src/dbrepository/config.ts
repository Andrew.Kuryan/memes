import * as dotenv from 'dotenv';
import { Sequelize } from 'sequelize-typescript';
import * as pg from 'pg';

import DBTag from '../db/models/DBTag';
import DBUser from '../db/models/DBUser';
import DBImage from '../db/models/DBImage';
import DBConstructedMem from '../db/models/DBConstructedMem';
import UserTag from '../db/models/UserTag';
import ImageTag from '../db/models/ImageTag';
import ConstructedMemTag from '../db/models/ConstructedMemTag';
import ImageLike from '../db/models/ImageLike';
import ConstructedMemLike from '../db/models/ConstructedMemLike';
import ConstructedTemplate from '../db/models/ConstructedTemplate';
import DBOAuthClient from '../db/models/DBOAuthClient';
import DBOAuthToken from '../db/models/DBOAuthToken';
import OAuthClientGrantType from '../db/models/OAuthClientGrantType';

dotenv.config();

export const models = {
    DBTag,
    DBUser,
    DBImage,
    DBConstructedMem,
    UserTag,
    ImageTag,
    ConstructedMemTag,
    ImageLike,
    ConstructedMemLike,
    ConstructedTemplate,
    DBOAuthClient,
    OAuthClientGrantType,
    DBOAuthToken,
};

pg.defaults.ssl = true;

export const sequelize = new Sequelize({
    host: 'ec2-34-230-149-169.compute-1.amazonaws.com',
    port: 5432,
    ssl: true,
    database: process.env.DB_NAME,
    dialect: 'postgres',
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    logging: false,
    models: Object.values(models),
});
