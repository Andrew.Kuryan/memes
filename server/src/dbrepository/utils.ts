import { differenceWith } from 'lodash';
import { isEmpty } from 'lodash';
import { Op } from 'sequelize';
import { DestroyOptions } from 'sequelize/types/lib/model';

export function listDifference<T>(
    oldList: Array<T>,
    newList: Array<T>,
    comparator: (first: T, second: T) => boolean,
): { toDelete: Array<T>; toCreate: Array<T> } {
    const toDelete = differenceWith(oldList, newList, comparator);
    const toCreate = differenceWith(newList, oldList, comparator);
    return { toDelete, toCreate };
}

export function whereDateRange(from?: Date, to?: Date): object {
    return {
        [Op.between]: [from ?? new Date(0), to ?? new Date(8640000000000000)],
    };
}

export function whereIncludesInArray<T, P>({
    pkey,
    inArr,
}: {
    pkey: { name: string; extractor: (el: T) => P };
    inArr?: Array<T>;
}): object {
    return inArr
        ? {
              where: { [pkey.name]: { [Op.or]: inArr.map(pkey.extractor) } },
          }
        : {};
}

export async function updateChildTable<T, PP, PC>({
    toDelete,
    toCreate,
    parentPkey,
    childPkey,
    destroy,
    createChildren,
}: {
    toDelete: Array<T>;
    toCreate: Array<T>;
    parentPkey: { name: string; value: PP };
    childPkey: { name: string; extractor: (el: T) => PC };
    destroy: (options?: DestroyOptions) => Promise<number>;
    createChildren: (parentPkey: PP, toCreate: Array<T>) => Promise<void>;
}) {
    if (!isEmpty(toDelete)) {
        await destroy({
            where: {
                [Op.and]: [
                    {
                        [parentPkey.name]: parentPkey.value,
                    },
                    {
                        [childPkey.name]: {
                            [Op.or]: toDelete.map(childPkey.extractor),
                        },
                    },
                ],
            },
        });
    }

    await createChildren(parentPkey.value, toCreate);
}
