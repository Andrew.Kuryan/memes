import OAuthClientRepository from '../appports/repository/OAuthClientRepository';
import OAuthClient, { OAuthClientData } from '../entity/OAuthClient';
import { models } from './config';
import { parseOauthClient } from './adapters/oauthClient';

export const oauthClientInclude = [models.OAuthClientGrantType];

export default class DBOAuthClientRepository implements OAuthClientRepository {
    async create(clientData: OAuthClientData): Promise<OAuthClient> {
        const result = await models.DBOAuthClient.build({
            name: clientData.getName(),
            ...(clientData.getAccessTokenLifetime()
                ? { accessTokenLifetime: clientData.getAccessTokenLifetime() }
                : {}),
            ...(clientData.getRefreshTokenLifetime()
                ? { refreshTokenLifetime: clientData.getRefreshTokenLifetime() }
                : {}),
            clientSecret: clientData.getClientSecret(),
            redirectUrl: clientData.getRedirectUrl(),
        }).save();
        for (const grantType of clientData.getGrantTypes()) {
            await models.OAuthClientGrantType.build({
                typeName: grantType,
                oauthClientId: result.id,
            }).save();
        }
        return Promise.resolve(new OAuthClient({ id: result.id, data: clientData }));
    }

    async getById(id: number): Promise<OAuthClient | null> {
        const dbOAuthClient = await models.DBOAuthClient.findByPk(id, {
            include: oauthClientInclude,
        });
        if (dbOAuthClient != null) {
            return Promise.resolve(parseOauthClient(dbOAuthClient));
        } else {
            return Promise.resolve(null);
        }
    }

    async remove(client: OAuthClient): Promise<OAuthClient> {
        await models.OAuthClientGrantType.destroy({
            where: {
                oauthClientId: client.getId(),
            },
        });
        const dbOAuthClient = await models.DBOAuthClient.findByPk(client.getId());
        await dbOAuthClient?.destroy();
        return Promise.resolve(client);
    }
}
