import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as morgan from 'morgan';
import * as cors from 'cors';

import {
    DBAccessTokenRepository,
    DBRefreshTokenRepository,
} from './dbrepository/DBOAuthTokenRepository';
import DBOAuthClientRepository from './dbrepository/DBOAuthClientRepository';
import DBUserRepository from './dbrepository/DBUserRepository';
import api from './application';
import DBMemRepository from './dbrepository/DBMemRepository';
import DBTemplateRepository from './dbrepository/DBTemplateRepository';
import DBTagRepository from './dbrepository/DBTagRepository';
import DBConstructedMemRepository from './dbrepository/DBConstructedMemRepository';

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan(':method :url; status::status; :response-time ms - res: :res[content-length]'));
app.use(cors());
app.use(express.static('./uploads/'));

app.use(
    '/api/v1',
    api(
        new DBAccessTokenRepository(),
        new DBRefreshTokenRepository(),
        new DBOAuthClientRepository(),
        new DBUserRepository(),
        new DBMemRepository(),
        new DBTemplateRepository(),
        new DBTagRepository(),
        new DBConstructedMemRepository(),
    ),
);

app.listen(3000, () => {
    console.log('Server lister on port 3000');
});

export default app;
