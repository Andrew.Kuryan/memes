import Mem from '../../entity/Mem';
import Template from '../../entity/Template';
import Image from '../../entity/Image';
import { serializeTag } from '../tags/adapter';
import { serializeUser } from '../users/adapter';

function serializeImage(image: Image) {
    return {
        id: image.getId(),
        creationDate: image.getCreationDate(),
        imageUrl: image.getData().getImageUrl(),
        tags: image
            .getData()
            .getTags()
            .map(serializeTag),
        author: serializeUser(image.getData().getAuthor()),
        likedUsers: image
            .getData()
            .getLikedUsers()
            .map(serializeUser),
    };
}

export function serializeTemplate(template: Template) {
    return serializeImage(template);
}

export function serializeMem(mem: Mem) {
    return serializeImage(mem);
}
