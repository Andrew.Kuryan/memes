import TemplateRepository from '../../appports/repository/TemplateRepository';
import { serializeTemplate } from './adapter';

import imageRoute from './ImageRoute';
import AuthMiddleware from '../AuthMiddleware';

export default (templateRepository: TemplateRepository, authMiddleware: AuthMiddleware) => {
    return imageRoute(templateRepository, '/templates', serializeTemplate, authMiddleware);
};
