import * as express from 'express';
import { Request, Response } from 'express';
import * as multer from 'multer';
import { replace } from 'lodash';

import ImageRepository from '../../appports/repository/ImageRepository';
import Image, { ImageData, ImageFilter, ImageSorter } from '../../entity/Image';
import {
    catchWrap,
    getOrThrow,
    getOrUndefined,
    paginationMiddleware,
    withPagination,
} from '../utils';
import { Pagination } from '../../appports/repository/Pagination';
import ApiError from '../../appports/repository/ApiError';
import User from '../../entity/User';
import Tag from '../../entity/Tag';
import AuthMiddleware from '../AuthMiddleware';

export default function<T extends Image>(
    imageRepository: ImageRepository<T>,
    path: string,
    serializer: (image: T) => object,
    authMiddleware: AuthMiddleware,
) {
    const router = express.Router();

    const storage = multer.diskStorage({
        destination: function(req, file, cb) {
            cb(null, `./uploads/images${path}`);
        },
        filename: function(req, file, cb) {
            cb(null, Date.now() + '-' + file.originalname);
        },
    });
    const upload = multer({ storage });

    const getImages = (filter: ImageFilter, sorter: ImageSorter) => async (
        req: Request,
        res: Response,
    ) => {
        const pagination: Pagination = res.locals.pagination;
        const { result: images, totalCount } = await imageRepository.search(
            filter,
            pagination,
            sorter,
        );
        res.json(
            withPagination({
                results: images.map(serializer),
                pagination,
                totalCount,
            }),
        );
    };

    router.get(path, paginationMiddleware, catchWrap(getImages(ImageFilter.EMPTY, ImageSorter.Id)));

    router.get(
        `${path}/latest`,
        paginationMiddleware,
        catchWrap(getImages(ImageFilter.EMPTY, ImageSorter.Date)),
    );

    router.get(
        `${path}/most-liked`,
        paginationMiddleware,
        catchWrap(getImages(ImageFilter.EMPTY, ImageSorter.Likes)),
    );

    router.get(
        `${path}/week-most-liked`,
        paginationMiddleware,
        catchWrap(
            getImages(
                new ImageFilter({
                    creationDateFrom: new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 7),
                }),
                ImageSorter.Likes,
            ),
        ),
    );

    router.get(
        `${path}/me`,
        authMiddleware(),
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            await getImages(
                new ImageFilter({
                    authorLogin: user.getLogin(),
                }),
                ImageSorter.Date,
            )(req, res);
        }),
    );

    router.get(
        `${path}/favorites`,
        authMiddleware(),
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            await getImages(
                new ImageFilter({
                    likedUsersLogin: [user.getLogin()],
                }),
                ImageSorter.Date,
            )(req, res);
        }),
    );

    router.post(
        `${path}/search`,
        paginationMiddleware,
        catchWrap(async (req, res) => {
            await getImages(
                new ImageFilter({
                    authorLogin: getOrUndefined(req.body, 'authorLogin'),
                    tagsName: getOrUndefined(req.body, 'tagsName'),
                    creationDateFrom: getOrUndefined(req.body, 'creationDateFrom'),
                    creationDateTo: getOrUndefined(req.body, 'creationDateTo'),
                    likedUsersLogin: getOrUndefined(req.body, 'likedUsersLogin'),
                }),
                ImageSorter.Id,
            )(req, res);
        }),
    );

    router.post(
        path,
        authMiddleware(),
        upload.fields([{ name: 'mem', maxCount: 1 }]),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const newData = new ImageData({
                author: user,
                imageUrl: replace(
                    (req.files as { mem: Array<any> })['mem'][0].path,
                    'uploads/',
                    '',
                ),
                tags: getOrThrow<string>(req.body, 'tagsName')
                    .split(',')
                    .map(t => new Tag(t)),
            });
            const image = await imageRepository.create(newData);
            res.json(serializer(image));
        }),
    );

    router.put(
        `${path}/:id`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await imageRepository.getById(id);
                if (
                    image
                        .getData()
                        .getAuthor()
                        .getLogin() === user.getLogin()
                ) {
                    const newData = new ImageData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags:
                            (req.body.tags as Array<string>)?.map(t => new Tag(t)) ??
                            image.getData().getTags(),
                        likedUsers: image.getData().getLikedUsers(),
                    });
                    const newImage = await imageRepository.update(image, newData);
                    res.json(serializer(newImage));
                } else {
                    throw new ApiError('Not an image author');
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.post(
        `${path}/:id/like`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await imageRepository.getById(id);
                if (
                    !image
                        .getData()
                        .getLikedUsers()
                        .find(u => u.getLogin() === user.getLogin())
                ) {
                    const newData = new ImageData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags: image.getData().getTags(),
                        likedUsers: [...image.getData().getLikedUsers(), user],
                    });
                    const result = await imageRepository.update(image, newData);
                    res.json(serializer(result));
                } else {
                    res.json(serializer(image));
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.delete(
        `${path}/:id/like`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await imageRepository.getById(id);
                if (
                    image
                        .getData()
                        .getLikedUsers()
                        .find(u => u.getLogin() === user.getLogin())
                ) {
                    const newData = new ImageData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags: image.getData().getTags(),
                        likedUsers: image
                            .getData()
                            .getLikedUsers()
                            .filter(u => u.getLogin() !== user.getLogin()),
                    });
                    const result = await imageRepository.update(image, newData);
                    res.json(serializer(result));
                } else {
                    res.json(serializer(image));
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.get(
        `${path}/:id`,
        catchWrap(async (req: Request, res: Response) => {
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await imageRepository.getById(id);
                res.json(serializer(image));
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    return router;
}
