import MemRepository from '../../appports/repository/MemRepository';
import { serializeMem } from './adapter';

import imageRoute from './ImageRoute';
import AuthMiddleware from '../AuthMiddleware';

export default (memRepository: MemRepository, authMiddleware: AuthMiddleware) => {
    return imageRoute(memRepository, '/memes', serializeMem, authMiddleware);
};
