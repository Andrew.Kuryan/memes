import ConstructedMem from '../../entity/ConstructedMem';
import { serializeUser } from '../users/adapter';
import { serializeTag } from '../tags/adapter';
import { serializeTemplate } from '../images/adapter';

export function serializeConstructedMem(constructedMem: ConstructedMem) {
    return {
        id: constructedMem.getId(),
        creationDate: constructedMem.getCreationDate(),
        author: serializeUser(constructedMem.getData().getAuthor()),
        imageUrl: constructedMem.getData().getImageUrl(),
        tags: constructedMem
            .getData()
            .getTags()
            .map(serializeTag),
        likedUsers: constructedMem
            .getData()
            .getLikedUsers()
            .map(serializeUser),
        templates: constructedMem
            .getData()
            .getTemplates()
            .map(serializeTemplate),
    };
}
