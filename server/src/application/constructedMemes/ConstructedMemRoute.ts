import * as express from 'express';
import { promisify } from 'util';
import * as fs from 'fs';

import ConstructedMemRepository from '../../appports/repository/ConstructedMemRepository';
import {
    catchWrap,
    getOrThrow,
    getOrUndefined,
    paginationMiddleware,
    withPagination,
} from '../utils';
import { Pagination } from '../../appports/repository/Pagination';
import { ConstructedMemData, ConstructedMemFilter } from '../../entity/ConstructedMem';
import { serializeConstructedMem } from './adapter';
import ApiError from '../../appports/repository/ApiError';
import AuthMiddleware from '../AuthMiddleware';
import User from '../../entity/User';
import Tag from '../../entity/Tag';
import ImageRepository from '../../appports/repository/ImageRepository';
import Template from '../../entity/Template';
import { ImageSorter } from '../../entity/Image';
import { Request } from 'express';
import { Response } from 'express';

export default (
    constructedMemRepository: ConstructedMemRepository,
    templatesRepository: ImageRepository<Template>,
    authMiddleware: AuthMiddleware,
) => {
    const router = express.Router();
    const writeFileAsync = promisify(fs.writeFile);

    const getImages = (filter: ConstructedMemFilter, sorter: ImageSorter) => async (
        req: Request,
        res: Response,
    ) => {
        const pagination: Pagination = res.locals.pagination;
        const { result: images, totalCount } = await constructedMemRepository.search(
            filter,
            pagination,
            sorter,
        );
        res.json(
            withPagination({
                results: images.map(serializeConstructedMem),
                pagination,
                totalCount,
            }),
        );
    };

    router.get(
        '/constructed-memes',
        paginationMiddleware,
        catchWrap(getImages(ConstructedMemFilter.EMPTY, ImageSorter.Id)),
    );

    router.get(
        `/constructed-memes/latest`,
        paginationMiddleware,
        catchWrap(getImages(ConstructedMemFilter.EMPTY, ImageSorter.Date)),
    );

    router.get(
        `/constructed-memes/most-liked`,
        paginationMiddleware,
        catchWrap(getImages(ConstructedMemFilter.EMPTY, ImageSorter.Likes)),
    );

    router.get(
        `/constructed-memes/week-most-liked`,
        paginationMiddleware,
        catchWrap(
            getImages(
                new ConstructedMemFilter({
                    creationDateFrom: new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 7),
                }),
                ImageSorter.Likes,
            ),
        ),
    );

    router.get(
        `/constructed-memes/me`,
        authMiddleware(),
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            await getImages(
                new ConstructedMemFilter({
                    authorLogin: user.getLogin(),
                }),
                ImageSorter.Date,
            )(req, res);
        }),
    );

    router.get(
        `/constructed-memes/favorites`,
        authMiddleware(),
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            await getImages(
                new ConstructedMemFilter({
                    likedUsersLogin: [user.getLogin()],
                }),
                ImageSorter.Date,
            )(req, res);
        }),
    );

    router.post(
        `/constructed-memes/search`,
        paginationMiddleware,
        catchWrap(async (req, res) => {
            await getImages(
                new ConstructedMemFilter({
                    authorLogin: getOrUndefined(req.body, 'authorLogin'),
                    tagsName: getOrUndefined(req.body, 'tagsName'),
                    creationDateFrom: getOrUndefined(req.body, 'creationDateFrom'),
                    creationDateTo: getOrUndefined(req.body, 'creationDateTo'),
                    likedUsersLogin: getOrUndefined(req.body, 'likedUsersLogin'),
                    templatesId: getOrUndefined(req.body, 'templatesId'),
                }),
                ImageSorter.Id,
            )(req, res);
        }),
    );

    router.post(
        '/constructed-memes',
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const fileData = getOrThrow<string>(req.body, 'data');
            const fileName = `images/constructed-memes/constructed-${Date.now()}.jpg`;
            await writeFileAsync(`./uploads/${fileName}`, Buffer.from(fileData, 'base64'));
            const templates = await Promise.all(
                getOrThrow<Array<number>>(req.body, 'templatesId').map(id =>
                    templatesRepository.getById(id),
                ),
            );
            const newData = new ConstructedMemData({
                author: user,
                imageUrl: fileName,
                tags: getOrThrow<Array<string>>(req.body, 'tagsName').map(t => new Tag(t)),
                templates,
            });
            const image = await constructedMemRepository.create(newData);
            res.json(serializeConstructedMem(image));
        }),
    );

    router.put(
        `/constructed-memes/:id`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await constructedMemRepository.getById(id);
                if (
                    image
                        .getData()
                        .getAuthor()
                        .getLogin() === user.getLogin()
                ) {
                    const newData = new ConstructedMemData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags:
                            (req.body.tags as Array<string>)?.map(t => new Tag(t)) ??
                            image.getData().getTags(),
                        likedUsers: image.getData().getLikedUsers(),
                        templates: image.getData().getTemplates(),
                    });
                    const newImage = await constructedMemRepository.update(image, newData);
                    res.json(serializeConstructedMem(newImage));
                } else {
                    throw new ApiError('Not an constructed mem author');
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.post(
        `/constructed-memes/:id/like`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await constructedMemRepository.getById(id);
                if (
                    !image
                        .getData()
                        .getLikedUsers()
                        .find(u => u.getLogin() === user.getLogin())
                ) {
                    const newData = new ConstructedMemData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags: image.getData().getTags(),
                        likedUsers: [...image.getData().getLikedUsers(), user],
                        templates: image.getData().getTemplates(),
                    });
                    const result = await constructedMemRepository.update(image, newData);
                    res.json(serializeConstructedMem(result));
                } else {
                    res.json(serializeConstructedMem(image));
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.delete(
        `/constructed-memes/:id/like`,
        authMiddleware(),
        catchWrap(async (req, res) => {
            const user: User = res.locals.oauth.token.user;
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const image = await constructedMemRepository.getById(id);
                if (
                    image
                        .getData()
                        .getLikedUsers()
                        .find(u => u.getLogin() === user.getLogin())
                ) {
                    const newData = new ConstructedMemData({
                        author: image.getData().getAuthor(),
                        imageUrl: image.getData().getImageUrl(),
                        tags: image.getData().getTags(),
                        likedUsers: image
                            .getData()
                            .getLikedUsers()
                            .filter(u => u.getLogin() !== user.getLogin()),
                        templates: image.getData().getTemplates(),
                    });
                    const result = await constructedMemRepository.update(image, newData);
                    res.json(serializeConstructedMem(result));
                } else {
                    res.json(serializeConstructedMem(image));
                }
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    router.get(
        '/constructed-memes/:id',
        catchWrap(async (req, res) => {
            const id = parseInt(req.params.id);
            if (!isNaN(id)) {
                const constructedMem = await constructedMemRepository.getById(id);
                res.json(serializeConstructedMem(constructedMem));
            } else {
                throw new ApiError('Id is not a number');
            }
        }),
    );

    return router;
};
