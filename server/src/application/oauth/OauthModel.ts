import {
    PasswordModel,
    RefreshTokenModel,
    Token,
    RefreshToken,
    Client,
    User as OAuthUser,
    Falsey,
} from 'oauth2-server';
import {
    AccessTokenRepository,
    RefreshTokenRepository,
} from '../../appports/repository/OAuthTokenRepository';
import OAuthClientRepository from '../../appports/repository/OAuthClientRepository';
import OAuthClient from '../../entity/OAuthClient';
import UserRepository from '../../appports/repository/UserRepository';
import { OAuthTokenData } from '../../entity/OAuthToken';
import User from '../../entity/User';
import sha256 = require('sha256');

export default class OAuthModel implements PasswordModel, RefreshTokenModel {
    private readonly accessTokenRepository: AccessTokenRepository;
    private readonly refreshTokenRepository: RefreshTokenRepository;
    private readonly oauthClientRepository: OAuthClientRepository;
    private readonly userRepository: UserRepository;

    constructor(
        accessTokenRepository: AccessTokenRepository,
        refreshTokenRepository: RefreshTokenRepository,
        oauthClientRepository: OAuthClientRepository,
        userRepository: UserRepository,
    ) {
        this.accessTokenRepository = accessTokenRepository;
        this.refreshTokenRepository = refreshTokenRepository;
        this.oauthClientRepository = oauthClientRepository;
        this.userRepository = userRepository;
    }

    async getAccessToken(accessToken: string): Promise<Token | Falsey> {
        const token = await this.accessTokenRepository.getByToken(accessToken);
        if (token != null) {
            const dbClient = token.getData().getClient();
            return Promise.resolve({
                accessToken: token.getData().getToken(),
                accessTokenExpiresAt: token.getData().getTokenExpiresOn(),
                client: OAuthModel.dbClientToClient(dbClient),
                user: token.getData().getUser(),
            });
        } else return Promise.resolve(null);
    }

    async getClient(clientId: string, clientSecret: string): Promise<Client | Falsey> {
        const client = await this.oauthClientRepository.getById(parseInt(clientId));
        if (client != null) {
            if (client.getData().getClientSecret() === sha256(`${clientId}:${clientSecret}`)) {
                return Promise.resolve(OAuthModel.dbClientToClient(client));
            } else {
                return Promise.resolve(null);
            }
        } else return Promise.resolve(null);
    }

    async getRefreshToken(refreshToken: string): Promise<RefreshToken | Falsey> {
        const token = await this.refreshTokenRepository.getByToken(refreshToken);
        if (token != null) {
            const dbClient = token.getData().getClient();
            return Promise.resolve({
                refreshToken: token.getData().getToken(),
                refreshTokenExpiresAt: token.getData().getTokenExpiresOn(),
                client: OAuthModel.dbClientToClient(dbClient),
                user: token.getData().getUser(),
            });
        } else return Promise.resolve(null);
    }

    async getUser(username: string, password: string): Promise<OAuthUser | Falsey> {
        try {
            const dbUser = await this.userRepository.getByLogin(username);
            if (dbUser.getPassword() === sha256(`${username}:${password}`)) {
                return Promise.resolve(dbUser);
            } else {
                return Promise.resolve(null);
            }
        } catch (exc) {
            return Promise.resolve(null);
        }
    }

    async revokeToken(token: RefreshToken | Token): Promise<boolean> {
        if (token.refreshToken != null) {
            const dbToken = await this.refreshTokenRepository.getByToken(token.refreshToken);
            if (dbToken != null) {
                await this.refreshTokenRepository.remove(dbToken);
                return Promise.resolve(true);
            } else return Promise.resolve(false);
        } else {
            const dbToken = await this.accessTokenRepository.getByToken(token.accessToken);
            if (dbToken != null) {
                await this.accessTokenRepository.remove(dbToken);
                return Promise.resolve(true);
            } else return Promise.resolve(false);
        }
    }

    async saveToken(token: Token, client: Client, user: OAuthUser): Promise<Token | Falsey> {
        const dbClient = await this.oauthClientRepository.getById(parseInt(client.id));
        if (dbClient != null) {
            let tokenExpiresOn: Date;
            if (token.accessTokenExpiresAt) {
                tokenExpiresOn = token.accessTokenExpiresAt;
            } else {
                const now = new Date();
                now.setHours(now.getHours() + 4);
                tokenExpiresOn = now;
            }
            await this.accessTokenRepository.create(
                new OAuthTokenData({
                    token: token.accessToken,
                    tokenExpiresOn,
                    client: dbClient,
                    user: user as User,
                }),
            );
            if (!token.refreshToken) {
                return Promise.resolve({
                    ...token,
                    client: OAuthModel.dbClientToClient(dbClient),
                    user: user,
                });
            } else {
                if (token.refreshTokenExpiresAt) {
                    tokenExpiresOn = token.refreshTokenExpiresAt;
                } else {
                    const now = new Date();
                    now.setHours(now.getHours() + 8);
                    tokenExpiresOn = now;
                }
                await this.refreshTokenRepository.create(
                    new OAuthTokenData({
                        token: token.refreshToken,
                        tokenExpiresOn,
                        client: dbClient,
                        user: user as User,
                    }),
                );
                return Promise.resolve({
                    ...token,
                    client: OAuthModel.dbClientToClient(dbClient),
                    user: user,
                });
            }
        } else return Promise.resolve(null);
    }

    verifyScope(token: Token, scope: string | string[]): Promise<boolean> {
        return Promise.resolve(true);
    }

    private static dbClientToClient(dbClient: OAuthClient): Client {
        return {
            id: dbClient.getId().toString(),
            redirectUris: dbClient.getData().getRedirectUrl() ?? undefined,
            grants: dbClient.getData().getGrantTypes(),
            accessTokenLifetime: dbClient.getData().getAccessTokenLifetime() ?? undefined,
        };
    }
}
