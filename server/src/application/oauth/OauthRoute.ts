import * as express from 'express';
import ExpressOAuthServer = require('express-oauth-server');

import OauthModel from './OauthModel';
import {
    AccessTokenRepository,
    RefreshTokenRepository,
} from '../../appports/repository/OAuthTokenRepository';
import OAuthClientRepository from '../../appports/repository/OAuthClientRepository';
import UserRepository from '../../appports/repository/UserRepository';

export default (
    accessTokenRepository: AccessTokenRepository,
    refreshTokenRepository: RefreshTokenRepository,
    oAuthClientRepository: OAuthClientRepository,
    userRepository: UserRepository,
) => {
    const router = express.Router();

    const oauth = new ExpressOAuthServer({
        model: new OauthModel(
            accessTokenRepository,
            refreshTokenRepository,
            oAuthClientRepository,
            userRepository,
        ),
        // @ts-ignore
        continueMiddleware: true,
        useErrorHandler: true,
    });

    router.post('/oauth/token', oauth.token());

    return { router, authMiddleware: oauth.authenticate.bind(oauth) };
};
