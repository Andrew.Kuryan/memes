import User from '../../entity/User';
import { serializeTag } from '../tags/adapter';

export function serializeUser(user: User) {
    return {
        login: user.getLogin(),
        email: user.getEmail(),
        avatarUrl: user.getAvatarUrl(),
        subscriptions: user.getSubscriptions().map(serializeTag),
    };
}
