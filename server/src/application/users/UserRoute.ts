import * as express from 'express';
import UserRepository from '../../appports/repository/UserRepository';
import { Request, Response } from 'express';
import { serializeUser } from './adapter';
import AuthMiddleware from '../AuthMiddleware';
import { catchWrap, getOrElse, getOrThrow, getOrUndefined } from '../utils';
import User from '../../entity/User';
import Tag from '../../entity/Tag';

export default (userRepository: UserRepository, authMiddleware: AuthMiddleware) => {
    const router = express.Router();

    router.get(`/users/me`, authMiddleware(), (req: Request, res: Response) => {
        const user: User = res.locals.oauth.token.user;
        res.json(serializeUser(user));
    });

    router.get(
        `/users/:login`,
        catchWrap(async (req: Request, res: Response) => {
            const user = await userRepository.getByLogin(req.params.login);
            res.json(serializeUser(user));
        }),
    );

    router.put(
        `/users/me`,
        authMiddleware(),
        catchWrap(async (req: Request, res: Response) => {
            const user: User = res.locals.oauth.token.user;
            const newUser = new User({
                login: user.getLogin(),
                avatarUrl: req.body.avatarUrl ?? user.getAvatarUrl(),
                password: req.body.password ?? user.getPassword(),
                email: req.body.email ?? user.getEmail(),
                subscriptions: req.body.subscriptions
                    ? (req.body.subscriptions as Array<string>).map(t => new Tag(t))
                    : user.getSubscriptions(),
            });
            const result = await userRepository.update(user, newUser);
            res.json(serializeUser(result));
        }),
    );

    router.post(
        '/users/register',
        catchWrap(async (req: Request, res: Response) => {
            const newUser = new User({
                login: getOrThrow(req.body, 'login'),
                avatarUrl: getOrUndefined(req.body, 'avatarUrl'),
                password: getOrThrow(req.body, 'password'),
                email: getOrThrow(req.body, 'email'),
                subscriptions: getOrElse<Array<string>>(req.body, 'subscriptions', []).map(
                    t => new Tag(t),
                ),
            });
            const result = await userRepository.create(newUser);
            res.json(serializeUser(result));
        }),
    );

    return router;
};
