import Tag from '../../entity/Tag';

export function serializeTag(tag: Tag) {
    return {
        name: tag.getName(),
    };
}
