import * as express from 'express';
import TagRepository from '../../appports/repository/TagRepository';
import { catchWrap, getOrThrow, paginationMiddleware, withPagination } from '../utils';
import { Pagination } from '../../appports/repository/Pagination';
import { serializeTag } from './adapter';
import Tag from '../../entity/Tag';
import ApiError from '../../appports/repository/ApiError';
import AuthMiddleware from '../AuthMiddleware';

export default (tagRepository: TagRepository, authMiddleware: AuthMiddleware) => {
    const router = express.Router();

    router.get(
        '/tags',
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const pagination: Pagination = res.locals.pagination;
            const tags = await tagRepository.getAll(pagination);
            res.json(
                withPagination({
                    results: tags.result.map(serializeTag),
                    pagination,
                    totalCount: tags.totalCount,
                }),
            );
        }),
    );

    router.get(
        '/tags/search',
        paginationMiddleware,
        catchWrap(async (req, res) => {
            const pagination: Pagination = res.locals.pagination;
            const tags = await tagRepository.search(pagination, new RegExp(req.query.regexp));
            res.json(
                withPagination({
                    results: tags.result.map(serializeTag),
                    pagination,
                    totalCount: tags.totalCount,
                }),
            );
        }),
    );

    router.get(
        '/tags/:name',
        catchWrap(async (req, res) => {
            const tag = await tagRepository.exists(req.params.name);
            if (tag) {
                res.json(serializeTag(new Tag(req.params.name)));
            } else {
                throw new ApiError('No such tag');
            }
        }),
    );

    router.post(
        '/tags',
        authMiddleware(),
        catchWrap(async (req, res) => {
            const newTag = new Tag(getOrThrow(req.body, 'name'));
            const result = await tagRepository.create(newTag);
            res.json(serializeTag(result));
        }),
    );

    return router;
};
