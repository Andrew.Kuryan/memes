import { NextFunction, Request, Response } from 'express';
import ApplicationError from './error/ApplicationError';
import ApiError from '../appports/repository/ApiError';
import { Pagination } from '../appports/repository/Pagination';

export function withPagination<T>({
    results,
    pagination,
    totalCount,
}: {
    results: Array<T>;
    pagination: Pagination;
    totalCount: number;
}) {
    return {
        results,
        pagination: {
            page: pagination.page,
            total_pages: Math.ceil(totalCount / pagination.limit),
        },
    };
}

export const paginationMiddleware = (req: Request, res: Response, next: NextFunction) => {
    const rawPagination = req.body.pagination ?? '{"limit": 25,"page":1}';
    res.locals.pagination = JSON.parse(rawPagination);
    next();
};

export const catchWrap = (fn: (req: Request, res: Response, next: NextFunction) => void) => async (
    req: Request,
    res: Response,
    next: NextFunction,
) => {
    try {
        await fn(req, res, next);
    } catch (error) {
        if (error instanceof ApiError) {
            next(new ApplicationError(400, error.message));
        } else {
            next(error);
        }
    }
};

export function getOrThrow<T>(obj: any, field: string): T {
    if (obj[field]) {
        return obj[field] as T;
    } else {
        throw new ApplicationError(400, `Missing required field ${field}`);
    }
}

export function getOrElse<T>(obj: any, field: string, defaultVal: T): T {
    if (obj[field]) {
        return obj[field] as T;
    } else {
        return defaultVal;
    }
}

export function getOrUndefined<T>(obj: any, field: string): T | undefined {
    if (obj[field]) {
        return obj[field] as T;
    } else {
        return undefined;
    }
}
