import { Request, Response, NextFunction, Router } from 'express';
import ApplicationError from './ApplicationError';

export default (router: Router) => {
    router.use((err: Error | ApplicationError, req: Request, res: Response, next: NextFunction) => {
        console.log(err.message);
        if (err instanceof ApplicationError) {
            res.status(err.getCode());
            res.json({
                error: true,
                message: err.message,
            });
        } else {
            res.status(500);
            res.json({
                error: true,
                message: err.message,
            });
        }
        res.end();
    });

    return router;
};
