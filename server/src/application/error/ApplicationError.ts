export default class ApplicationError extends Error {
    private readonly code: number;

    constructor(code: number, message: string) {
        super(message);
        this.code = code;
    }

    getCode(): number {
        return this.code;
    }
}
