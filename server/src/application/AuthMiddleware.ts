import * as express from 'express';

type AuthMiddleware = (
    scope?: string,
) => (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction,
) => Promise<void>;

export default AuthMiddleware;
