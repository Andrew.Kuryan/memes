import * as express from 'express';
import {
    AccessTokenRepository,
    RefreshTokenRepository,
} from '../appports/repository/OAuthTokenRepository';
import OAuthClientRepository from '../appports/repository/OAuthClientRepository';
import MemRepository from '../appports/repository/MemRepository';
import UserRepository from '../appports/repository/UserRepository';
import TemplateRepository from '../appports/repository/TemplateRepository';
import TagRepository from '../appports/repository/TagRepository';
import ConstructedMemRepository from '../appports/repository/ConstructedMemRepository';

import oauth from './oauth/OauthRoute';
import errorHandler from './error/ErrorRoute';
import memRoute from './images/MemRoute';
import templateRoute from './images/TemplateRoute';
import userRoute from './users/UserRoute';
import tagRoute from './tags/TagRoute';
import constructedMemRoute from './constructedMemes/ConstructedMemRoute';

export default (
    accessTokenRepository: AccessTokenRepository,
    refreshTokenRepository: RefreshTokenRepository,
    oAuthClientRepository: OAuthClientRepository,
    userRepository: UserRepository,
    memRepository: MemRepository,
    templateRepository: TemplateRepository,
    tagRepository: TagRepository,
    constructedMemRepository: ConstructedMemRepository,
) => {
    const router = express.Router();

    const { router: oauthRoute, authMiddleware } = oauth(
        accessTokenRepository,
        refreshTokenRepository,
        oAuthClientRepository,
        userRepository,
    );

    router.use(oauthRoute);
    router.use(memRoute(memRepository, authMiddleware));
    router.use(templateRoute(templateRepository, authMiddleware));
    router.use(userRoute(userRepository, authMiddleware));
    router.use(tagRoute(tagRepository, authMiddleware));
    router.use(constructedMemRoute(constructedMemRepository, templateRepository, authMiddleware));

    return errorHandler(router);
};
